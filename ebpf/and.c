#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>


int a=1,b=2;

int and(int x,int y){
    return x+y;
}
int minus(int x,int y){
   return x-y;
}
int main(int argc,char *argv[]){
    if(argc>=2){
    	 a=atoi(argv[1]);
    }
    if(argc>=3){
	   b=atoi(argv[2]);
    }

    int c;
    printf("pid=%d\n",getpid());
    c=and(a,b);
    printf("c=%d\n",c);
    int d;
    d=minus(a,b);
    printf("d=%d\n",d);

    return 0;
}
