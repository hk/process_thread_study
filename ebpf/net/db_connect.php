<?php
echo posix_getpid();
echo PHP_EOL;


$host = '139.198.170.149';
$user = 'root';
$psd = '2020hkui';

$redisConf = [
    'host' => '127.0.0.1',
    'port' => 6379,
    'password' => '123456'

];

function getMysql()
{
    global $host, $user, $psd;
    try {
        $mysql = new PDO("mysql:host={$host};port=3306;dbname=test;charset=UTF8", $user, $psd);
        $mysql->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $mysql;
    } catch (\Exception $e) {
        print_r($e->getMessage());
        return false;
    }
}

function getRedis()
{
    $redis = new \Redis();
    global $redisConf;
    try {
        $redis->connect($redisConf['host'], $redisConf['port'], 3);
        $redis->auth($redisConf['password']);
    } catch (\RedisException $re) {
        throw new \RedisException("redis 连接失败" . $re->getMessage());
    }
    $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
    return $redis;
}


$conn1=getMysql();
$conn2=getMysql();

$red1=getRedis();

