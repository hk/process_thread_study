<?php
$host = '0.0.0.0';
$port = 9999;
$listen_socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
socket_bind( $listen_socket, $host, $port );
socket_listen( $listen_socket );
cli_set_process_title( "Yaw Master Process" );
// fork出10个子进程
// 子进程会继承父进程中$listen_socket
// 因此每个子进程都以accept客户端了
for( $i = 1; $i <= 10; $i++ ) {
    $pid = pcntl_fork();
    if ( 0 == $pid ) {
        cli_set_process_title( "Yaw Worker Process" );
        while ( true ) {
            $connection_socket = socket_accept( $listen_socket );
            socket_recv( $connection_socket, $recv_content, 4, MSG_WAITALL );
            $body_len_pack = unpack( "Nlen", $recv_content );
            $body_len      = $body_len_pack['len'];
            socket_recv( $connection_socket, $recv_content, $body_len, MSG_WAITALL );
            echo posix_getpid().PHP_EOL;
            socket_close( $connection_socket );
        }
    }
}

while( true ) {
    sleep( 1 );
}