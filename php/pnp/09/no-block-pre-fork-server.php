<?php
$host = '0.0.0.0';
$port = 9999;
$listen_socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
socket_bind( $listen_socket, $host, $port );
socket_listen( $listen_socket );

// 将listen_socket设置为非阻塞
socket_set_nonblock( $listen_socket );

cli_set_process_title( "Yaw Master Process" );

for( $i = 1; $i <= 50; $i++ ) {
    $pid = pcntl_fork();
    if ( 0 == $pid ) {
        cli_set_process_title( "Yaw Worker Process" );
        while ( true ) {

            // 此时accept就会不断不断不断不断进行...
            // 由于没有连接，accept会返回false
            // 所以此处需要我们判断一下
            // 如果你想参观一下非阻塞的不断不断不断不断
            // 那么你可以人为echo输出一些东西。。感受下？？？
            $connection_socket = socket_accept( $listen_socket );
            if ( !$connection_socket ) {
                echo socket_last_error($listen_socket);
                continue;
            }

            socket_recv( $connection_socket, $recv_content, 4, MSG_WAITALL );
            $body_len_pack = unpack( "Nlen", $recv_content );
            $body_len      = $body_len_pack['len'];
            socket_recv( $connection_socket, $recv_content, $body_len, MSG_WAITALL );
            echo posix_getpid().PHP_EOL;
            socket_close( $connection_socket );
        }
    }
}
while( true ) {
    sleep( 1 );
}