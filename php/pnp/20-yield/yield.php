
<?php
$start_mem = memory_get_usage();
function yield_range( $start, $end ){
    while( $start <= $end ){
        $start++;
        yield $start;
    }
}
foreach( yield_range( 0, 9999 ) as $item ){
    //echo $item.',';
}
$end_mem = memory_get_usage();
echo " use mem : ".(( $end_mem - $start_mem )/1024).'bytes'.PHP_EOL;