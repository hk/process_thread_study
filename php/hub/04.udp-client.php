<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/5/21
 * Time: 22:04
 */
$ip = "127.0.0.1";
$port = $argv[1];
$socketfd = socket_create(AF_INET,SOCK_DGRAM,SOL_UDP);

$msgStruct = [
    'name' =>[
        'addr'=>$ip,
        'port'=>$port
    ],
    'control' =>[],
    'iov' => ["hello,world"],
    'flags'=> 0
];

while (1){
//阻塞读取终端的输入
    $data = fread(STDIN,8192);
    if ($data){
        $recvMsg="";$remoteIp;$remoteAddr;
        socket_sendto($socketfd,$data,strlen($data),0,$ip,$port);
        socket_recvfrom($socketfd,$recvMsg,8192,0,$remoteIp,$remoteAddr);
        fprintf(STDOUT,"ip:%s,port:%d,msg:%s\n",$remoteIp,$remoteAddr,$recvMsg);
    }
}

socket_close($socketfd);