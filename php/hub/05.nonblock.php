<?php
/**
 * Created by PhpStorm.
 * User: huangkui@lepu.cn
 * Date: 2020/5/23
 * Time: 10:42
 */
$ip = "0.0.0.0";
$port = $argv[1];
$sockefd = socket_create(AF_INET,SOCK_STREAM,0);

socket_bind($sockefd,$ip,$port);
socket_listen($sockefd,5);

$counter = 0;
while (1){

    $counter++;
    //$sockfd 我们最好让它阻塞，不然立马返回的话 $connfd可不是资源即连接就会导致socket_set_nonblock报错的
    $connfd = socket_accept($sockefd);
    echo "accept \n";
    //得到客户端连接设置为非阻塞IO模式
    socket_set_nonblock($connfd);
    if ($connfd){
        socket_write($connfd,"hello,php 是世界上是好的语言");
        echo "write once\n";
        //要是这里不加while它就跑一次完事了
        //加上while【忙轮询】就是让socket_read一直不停的问有没有数据
        while (1){
            if(($recv=socket_read($connfd,10,PHP_BINARY_READ))){
                echo "recv=".$recv.PHP_EOL;
                socket_write($connfd,"server:$recv");
            }
            //上面的socket_read不管有没有读取到数据
            //都立马返回并且执行echo
            //如果是阻塞模式，则该句就得等上面的socket_read运行了这里才执行，否则就得等，等的结果就是SLEEP，cpu就会干其它事情了
            echo "不阻塞一直不停的执行socket_read问内核有没有数据\n";
            sleep(1);
        }
    }

}
socket_close($sockefd);
socket_close($connfd);