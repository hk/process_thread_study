<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/5/21
 * Time: 21:57
 */
$ip = "0.0.0.0";
$port = $argv[1];
$sockefd = socket_create(AF_INET,SOCK_DGRAM,SOL_UDP);
socket_set_option($sockefd,SOL_SOCKET,SO_REUSEPORT,1);
socket_bind($sockefd,$ip,$port);
function resetClient()
{
//此结构PHP.net手册并没有细说，而是本人通过调试低层内核代码获取以及参考unix API
//大家可以自行参考
    return  [
        'name' =>[],//socket 套接字地址 【有通用地址和专用地址】 协议不同 此参数内容结构不同，后面扯
        'control' =>[],//
        'iov' => [],//数据 可参考 struct iovec
        'flags'=> 0,//接收消息标志位  给0正常就行
        'controllen'=>8192//辅助数据的地址
    ];
}

while(1){
    $client=resetClient();
    //在此阻塞SLEEPING
    socket_recvmsg($sockefd,$client,0);
    $name=$client['name'];

    if (isset($client['name']['addr'])){
        $addr=$client['name']['addr'];
        $port=$client['name']['port'];
        $msg="data from ".$addr.":".$port." ".date("Y-m-d H:i:s");
        socket_sendto($sockefd,$msg,strlen($msg),0,$addr,$port);
    }

    sleep(2);
}

socket_close($sockefd);