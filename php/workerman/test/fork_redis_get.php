<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/1
 * Time: 20:21
 */

$o_redis = new Redis();
$o_redis->connect( '127.0.0.1', 6379 );


for ( $i = 1; $i <= 5; $i++ ) {
    $i_pid = pcntl_fork();
    if ( 0 == $i_pid ) {
        if($i>1){
            sleep(5);
        }
        var_dump($o_redis->ping());
        $b_ret = $o_redis->sismember( "uid", $i );
        echo posix_getpid()."--".$i.':'.var_export( $b_ret,1 ). " clients count=".count($o_redis->client('list')).PHP_EOL;
        if($i==1){
            $o_redis->close();
            exit(1);
        }
        while( true ) {
            sleep( 1 );
        }
    }
}

while($r=pcntl_wait($status)){
    if($r<0){
        echo "error:".pcntl_strerror(pcntl_get_last_error());
        exit();
    }
    echo "wait====".$r."--".$status."---".pcntl_wexitstatus($status).PHP_EOL;
}