<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/1
 * Time: 17:48
 * ps --ppid 31972 -o ppid,pid
 */
echo "###".posix_getpid().PHP_EOL;
for ( $i = 1; $i <= 3; $i++ ) {
    $i_pid = pcntl_fork();
    if ( 0 == $i_pid ) {
        echo "@子进程 i===".$i."----".posix_getpid().'--'.posix_getppid().PHP_EOL;
        exit();
    }
}

sleep(100);
