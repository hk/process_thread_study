<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/1
 * Time: 20:21
 */

$o_redis = new Redis();
$o_redis->connect( '127.0.0.1', 6379 );

$key='hk';
for ( $i = 1; $i <= 4; $i++ ) {
    $i_pid = pcntl_fork();
    if ( 0 == $i_pid ) {
        $b_ret = $o_redis->sAdd($key,$i."-".posix_getpid());
        var_dump($b_ret);
        // 使用while保证三个子进程不会退出...
        while( true ) {
            sleep( 1 );
        }
    }
}
// 使用while保证主进程不会退出...
while( true ) {
    sleep( 1 );
}