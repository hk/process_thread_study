<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/1
 * Time: 23:36
 */

for ( $i = 1; $i <= 100; $i++ ) {
    $i_pid = pcntl_fork();
    if ( 0 == $i_pid ) {
        file_put_contents( "./Core.log", $i.PHP_EOL, FILE_APPEND );
        // 使用while保证三个子进程不会退出...
        while( true ) {
            sleep( 1 );
        }
    }
}
// 使用while保证主进程不会退出...
while( true ) {
    sleep( 1 );
}