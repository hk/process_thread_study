<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/1
 * Time: 23:33
 */



$o_redis = new Redis();

for ( $i = 1; $i <= 4; $i++ ) {
    $i_pid = pcntl_fork();
    if ( 0 == $i_pid ) {
        $o_redis->connect( '127.0.0.1', 6379 );
        $b_ret = $o_redis->sismember( "uid", $i );
        echo $i.':'.json_encode( $b_ret ).PHP_EOL;
        // 使用while保证三个子进程不会退出...
        while( true ) {
            sleep( 1 );
        }
    }
}
// 使用while保证主进程不会退出...
while( true ) {
    sleep( 1 );
}