<?php
/**
 * 1.信号会打断sleep
 *
 */

pcntl_async_signals(true);
//父子进程均得到该信号
pcntl_signal(SIGINT,'sigfun',false); //false与true区别
pcntl_signal(SIGUSR1,'sigfun',false); //false与true区别

echo "#######".posix_getpid().'##########'.PHP_EOL;

cli_set_process_title("hk_parent:".posix_getpid());


$work_num=1;

$workers=[];

for($i=0;$i<$work_num;$i++){
    $pid=pcntl_fork();
    if($pid<0) exit("fork err");
    if($pid==0){
        workerFun();
        exit();
    }else{
        $workers[]=$pid;
    }
}
print_r($workers);


while(1){
    $pid=pcntl_wait($status,WUNTRACED);
    $errno=pcntl_get_last_error();
    //No child processes
    if($errno ==10){
        break;
    }
    echo $errno."-------------------------------------------".pcntl_strerror($errno).PHP_EOL;

    if($pid>0){
        echo $pid." exited!".PHP_EOL;
    }
}
function workerFun(){
    $son_pid=posix_getpid();
    cli_set_process_title("hk_son:".$son_pid);


    $eventConfig=new EventConfig();
    $eventBase=new EventBase($eventConfig);
    $i=0;
    pcntl_signal(SIGINT,SIG_IGN,false);
    $signalEvent=Event::signal($eventBase,SIGINT,function ($signo)use(&$i){
        echo posix_getpid()." event-signo:".$signo."--".$i.PHP_EOL;
        $i++;
        if($i>5){
            exit(10);
        }
    });
    $signalEvent->add();
    $eventBase->loop();

}

function sigfun($signo){
    echo posix_getpid()." get signum: ".$signo."----".date("H:i:s").PHP_EOL;

}
