<?php

/**
 * openssl genrsa -out rsa_private_key.pem 1024
 * openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
 *
 *
 */

include 'RSA.php';


$path=dirname(__FILE__);
$publicKeyFile1=$path.'/rsa1/rsa_public_key.pem';
$privateKeyFile1=$path.'/rsa1/rsa_private_key.pem';


$publicKeyFile2=$path.'rsa2/rsa_public_key.pem';
$privateKeyFile2=$path.'rsa2/rsa_private_key.pem';

$rsa=new RSA($publicKeyFile1,$privateKeyFile1);

$data      = 'abcdefg';
$encrypted = $rsa->encrypt( $data );
$decrypted = $rsa->decrypt( $encrypted );
echo "enc=".$encrypted.PHP_EOL;
echo "dec=".$decrypted.PHP_EOL;



