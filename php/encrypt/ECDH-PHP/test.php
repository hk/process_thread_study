<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/3/19
 * Time: 22:27
 */
require_once './autoloader.php';
use Querdos\lib\ECDHCurve25519;
$xitele   = new ECDHCurve25519();
$gudelian = new ECDHCurve25519();
die;
$xitele->computeSecret( $gudelian->getPublic() );
$gudelian->computeSecret( $xitele->getPublic() );
// shareKey1 和 shareKey2 就是协商出来的密钥
$shareKey1 = $xitele->getSecret();
echo $shareKey1.PHP_EOL;
$shareKey2 = $gudelian->getSecret();
echo $shareKey2.PHP_EOL;
// 我们用gmp cmp来对比是否为同一个密钥
if ( 0 == gmp_cmp( $shareKey1, $shareKey2 ) ) {
    echo "一样".PHP_EOL;
}
else {
    echo "不一样".PHP_EOL;
}
// 除此之外，ecdh比dh多了一个验证数据签名验证，也就是说ecdh可以检验数据是否被篡改！
$msg = "hello world";
$signature = $xitele->signMessage( $msg );
if ( $gudelian->verifySignature( $signature, $xitele->getPublic(), $msg ) ) {
    echo "验证数据签名成功".PHP_EOL;
}
else {
    echo "验证数据签名失败".PHP_EOL;
}
exit;