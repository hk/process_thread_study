<?php
pcntl_async_signals(true);
$pid=posix_getpid();
echo "pid=".$pid.PHP_EOL;

$restart_syscalls=false;
pcntl_signal(SIGCHLD,function ($signo){
    echo posix_getpid(). " get signo=".$signo.PHP_EOL;
},$restart_syscalls);
pcntl_signal(SIGUSR1,function ($signo){
    echo posix_getpid()."  get signo=".$signo.PHP_EOL;
},$restart_syscalls);
pcntl_signal(SIGINT,function ($signo){
    echo posix_getpid()."  get signo=".$signo.PHP_EOL;
    exit();
},$restart_syscalls);

fork();
echo "end".PHP_EOL;
function fork(){
    $pid=pcntl_fork();
    if($pid<0){
        exit(250);
    }
    //son
    if($pid==0){
        echo "son=".posix_getpid().PHP_EOL;
        sleep(1000);
        echo "after sleep".PHP_EOL;
        exit();
    }else{
        //parent
        while(true){
            $wpid=pcntl_wait($status);

            echo "wait pid=".$wpid.PHP_EOL;
            if($wpid>0){
                break;
            }
        }
    }
}