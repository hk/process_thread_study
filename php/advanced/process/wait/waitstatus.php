<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/14
 * Time: 19:58
 */

$i_pid = pcntl_fork();
if ( 0 == $i_pid ) {
    for( $i = 1; $i <= 10; $i++ ) {
        echo "子进程 ".posix_getpid()." running".PHP_EOL;
        sleep( 1 );
    }
    exit( 11 );
}
echo '父进程block在此'.PHP_EOL;
$i_ret = pcntl_wait( $status, WUNTRACED );
echo $i_ret." : ".$status.PHP_EOL;
$ret = pcntl_wexitstatus( $status );//退出码11
var_dump( $ret );
$ret = pcntl_wifexited( $status );
var_dump( $ret );
$ret = pcntl_wifsignaled( $status );//检查子进程是否因信号而中断
var_dump( $ret );
if($ret){
    echo "signo:".pcntl_wtermsig($status).PHP_EOL;
}
$ret = pcntl_wifstopped( $status );//用于检测子进程是否已停止
var_dump( $ret );