<?php
$i_pid = pcntl_fork();
if ( 0 == $i_pid ) {
    for( $i = 1; $i <= 300; $i++ ) {
        echo posix_getpid()." child alive".PHP_EOL;
        sleep( 1 );
    }
    exit;
}

while( true ) {
    $i_ret = pcntl_waitpid( 0, $status, WNOHANG | WUNTRACED|WCONTINUED  );
    $ret = pcntl_wifstopped( $status );
    echo "是否停止:".json_encode( $ret ).PHP_EOL;
    sleep( 1 );
}
/**
kill -STOP 31381
kill -CONT 31381
 */