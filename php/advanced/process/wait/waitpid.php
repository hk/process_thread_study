<?php
$pid=pcntl_fork();
$child=0;
if($pid==0){
	cli_set_process_title("php_son:".posix_getpid());
	echo "son:".posix_getpid().PHP_EOL;
	for($i=0;$i<3;$i++){
		echo 'son->'.$i.PHP_EOL;
		sleep(1);
	}
}elseif($pid>0){
	cli_set_process_title("php_parent:".posix_getpid());
	echo "parent:".posix_getpid().PHP_EOL;

	$wait_res=pcntl_waitpid($pid, $status,WUNTRACED );
	$wifexited=pcntl_wifexited($status);
	echo "pcntl_waitpid=".var_export($wait_res,true)."------>status=".var_export($status,true)."-->wifexited=".$wifexited.PHP_EOL;
	for($i=0;$i<10;$i++){
		echo 'parent->'.$i.PHP_EOL;
		sleep(1);
	}
	

}else{
	echo "fork_err";
}
echo "out-------------------->".posix_getpid().PHP_EOL;

/*
for i in `seq 1 20 ` ;do ps aux|grep php;echo $i'--------------------------------';sleep 0.5;done;
*/