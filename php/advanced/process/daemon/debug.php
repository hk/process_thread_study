<?php
if($argc<2){
    echo "need args";
    exit();
}
$pid=$argv[1];


function getChilds($pid){
    $pids=[];
    $cmd="ps --ppid {$pid}|awk '/[0-9]/{print $1}'|xargs";
    //echo $cmd.PHP_EOL;
    exec($cmd, $output, $status);

    $output=array_filter($output);



    if($status==0 && !empty($output)){
        foreach ($output as $item_pid){
            $item_pid_arr=explode(' ',$item_pid);
            foreach ($item_pid_arr as $item_son_id){
                $childs=getChilds($item_son_id);
                $pids[$item_son_id]=$childs;
            }


        }
    }

    return $pids;

}

$res=getChilds($pid);

print_r($res);

