<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/14
 * Time: 21:52
 */
function daemonize() {
    // 设置权限掩码,umask大家可以搜一下
    umask( 0 );
    // 将目录更换到指定某个目录，一般是根目录
    // 如果不更换，存在一种问题就是：daemon进程默认目录无法被卸载unmount
    chdir( '/' );
    $i_pid = pcntl_fork();
    // 在子进程中...
    if ( 0 == $i_pid ) {
        // setsid创建新会话组
        if ( posix_setsid() < 0 ) {
            exit();
        }
        // 在子进程中二次fork()，这里据说是为了避免SVR4种一次fork有时候无法脱离控制终端
        $i_pid = pcntl_fork();
        if ( $i_pid > 0  ) {
            exit;
        }
        // 关闭 标准输入
        // 这里仅仅是关闭，你可以根据你的需要重定向到其他位置，比如某些文件
        fclose( STDOUT );
    }
    // 父进程退出
    else if ( $i_pid > 0 ) {
        exit();
    }
}
// 首先执行daemonize函数，使得进程daemon化
daemonize();
// 连接redis，在后台做一些事情

$o_redis = new Redis();
$o_redis->connect( '127.0.0.1', 6379 );
while ( true ) {
    echo $o_redis->get( 'user:1' ).PHP_EOL;
    sleep( 1 );
}