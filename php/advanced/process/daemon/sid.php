<?php
echo posix_getpid().'####'.PHP_EOL;

$pid = pcntl_fork();
if ( 0 == $pid ) {
    $ppid = pcntl_fork();
    if ( 0 == $ppid ) {
        while ( true ) {
            sleep( 1 );
        }
    }
    while ( true ) {
        sleep( 1 );
    }
}
while ( true ) {
    sleep( 1 );
}
//ps -eo pid,ppid,pgid,sid,command |awk '$5=="php"||$1=="PID" {print $0}'

/**
[root@hkui daemon]# ps -eo pid,ppid,pgid,sid,command |awk '$5=="php"||$1=="PID" {print $0}'
PID  PPID  PGID   SID COMMAND
11402 27954 11402 27954 php sid.php
11403 11402 11402 27954 php sid.php
11404 11403 11402 27954 php sid.php
[root@hkui daemon]# ps -eo pid,ppid,pgid,sid,command |awk '$1==27954||$1=="PID" {print $0}'
PID  PPID  PGID   SID COMMAND
27954 27951 27954 27954 -bash

 */