<?php
$file='/tmp/fifo';

if(!file_exists($file)){
    if(!posix_mkfifo($file,0664)){
        die("create fifo ".$file.' Fail'.PHP_EOL);
    }
}

$i=0;
while(1){
    $i++;
    $handle=fopen($file,'w');
    fwrite($handle,"[$i]");
    sleep(1);
    if($i==5) break;
}

/**
[root@hkui ~]# ll /tmp/fifo
prw-r--r-- 1 root root 0 May  9 22:49 /tmp/fifo

 */