<?php
$descriptorspec = array(
    0 => array("pipe", "r"),  // 标准输入，子进程从此管道中读取数据
    1 => array("pipe", "w"),  // 标准输出，子进程向此管道中写入数据
    2 => array("file", "/tmp/error-output.txt", "a") // 标准错误，写入到一个文件
);

$cwd = '.';


$process = proc_open('php', $descriptorspec, $pipes, $cwd);

if (is_resource($process)) {

    fwrite($pipes[0], '<?php echo date("Y-m-d H:i:s"); ?>');

    fclose($pipes[0]);

    $str= stream_get_contents($pipes[1]).PHP_EOL;
    fclose($pipes[1]);
    echo $str;


    // 切记：在调用 proc_close 之前关闭所有的管道以避免死锁。
    $return_value = proc_close($process);

    echo "returned $return_value\n";
}