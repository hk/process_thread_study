<?php
/**
 * 双工通信
 */

$pipe_file = __DIR__.DIRECTORY_SEPARATOR.'test.pipe';
pcntl_signal(SIGINT,function ($signo)use($pipe_file){
    echo '---------get sig '.$signo.'---------------------'.PHP_EOL;
    if(file_exists($pipe_file)){
        unlink($pipe_file);

    }

});

if( !file_exists( $pipe_file ) ){
    if( !posix_mkfifo( $pipe_file, 0666 ) ){
        exit( 'create pipe error.'.PHP_EOL );
    }
}
