<?php

$cmd='cat';
$desc=[
    0 => ["pipe", "r"],  // 标准输入，子进程从此管道中读取数据
    1 => ["pipe", "w"], // 标准输出，子进程向此管道中写入数据
    2 => ["file", "/tmp/error-output.txt", "a"] // 标准错误，写入到一个文件
];
$cwd='/tmp';

$process=[];

for($i=0;$i<3;$i++){
    $proc=proc_open($cmd,$desc,$pipes);
    fwrite($pipes[0],$i."--".mt_rand(0,9999999));
    $process[]=[
        'proc'=>$proc,
        'output'=>$pipes[1]
    ];
    fclose($pipes[0]);
}

foreach ($process as $proc){
    echo fread($proc['output'],1024).PHP_EOL;
    fclose($proc['output']);
    $return=proc_close($proc['proc']);
}




