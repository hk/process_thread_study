<?php
/**
 * 僵尸进程
 * 父进程回收
 *
 */
$pid=posix_getppid();

$pid=pcntl_fork();
if($pid>0){
    cli_set_process_title("hk_parent:".posix_getpid());

    $i=0;
    while ($i<10){
        sleep(1);
        $i++;
        echo "parent:".posix_getpid().PHP_EOL;
        system("ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]'");
    }
    $res=pcntl_wait($status,WUNTRACED); //返回子进程pid

    $res1=pcntl_wifexited($status);
    var_dump($res1);


}else if($pid==0){
    cli_set_process_title("hk_son:".posix_getpid());
    $i=0;
    while($i<5){
        sleep(1);
        $i++;
        echo "son:".posix_getpid().PHP_EOL;
        system("ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]'");
    }
    exit(1);
}else{
	echo " fork error";
}
//子进程10s退出时，父进程未对其回收

//ps -A -ostat,ppid,pid,cmd |grep -e '^[Zz]' 查看僵尸进程
