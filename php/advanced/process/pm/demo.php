<?php
include 'job.php';
include 'Process.php';


class demo extends Process
{
    public function runJob($worker, $index)
    {
        \swoole_set_process_name(sprintf('%s-worker-%d', $this->process_name,$index));
        $job=new job();
        $job->do($this);
    }
}

if(count($argv)<3){
    exit( "params lost".PHP_EOL);
}
$process_name=$argv[1];

$worker_num=1;

$cmd=$argv[2];

$cmd2=$argv[3]??'';



$config=[
    'process_name'=>$process_name,
    'worker_num'=>$worker_num,
    'out_file'=>'/tmp/out',
    'max_run_time'=>10 , //运行10个后自己退出，然后master补上
    'daemon'=> $cmd2=='-d'?true:false

];
$process=new demo($config);
echo posix_getpid().PHP_EOL;
$process->run($cmd);
