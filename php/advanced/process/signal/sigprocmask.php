<?php
echo posix_getpid().PHP_EOL;

pcntl_async_signals( true );
// 信号处理器...
function signal_handler( $i_signo ) {
    switch ( $i_signo ) {
        case SIGUSR2:
            echo "sigusr2信号.".PHP_EOL;
            break;
        case SIGUSR1:
            echo "sigusr1信号.".PHP_EOL;
            break;
        default:
            echo "未知信号.".PHP_EOL;
    }
}
// 给进程安装信号...
pcntl_signal( SIGUSR1, "signal_handler" );
pcntl_signal( SIGUSR2, "signal_handler" );
pcntl_sigprocmask( SIG_BLOCK, array( SIGUSR2,SIGUSR1 ),$a_oldset ); //$a_old_set为这个执行前 的阻塞信号集合，这里为空数组




// while保持进程不要退出..
$i_counter = 0;
while ( true ) {
    $i_counter++;
    sleep( 1 );
    echo $i_counter.PHP_EOL;
    if ( 15 == $i_counter ) {
        pcntl_sigprocmask( SIG_UNBLOCK, array( SIGUSR1 ), $a_oldset ); //[10,12]
        print_r($a_oldset);
    }

}