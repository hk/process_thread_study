<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/12/13
 * Time: 23:06
 */

echo posix_getpid().PHP_EOL;
pcntl_async_signals(true);
//父子进程均得到该信号
pcntl_signal(SIGINT,'sigfun');


function sigfun($signo){
    echo posix_getpid()."--signo--".date("H:i:s").PHP_EOL;

}
$workers=[];
for($i=0;$i<2;$i++){
    $pid=pcntl_fork();
    if($pid<0)die();
    if($pid==0){
//        pcntl_signal(SIGINT,SIG_DFL);
        for($j=0;$j<3;$j++){
            echo posix_getpid()."--".$j.PHP_EOL;
            sleep(1);
        }
        exit(1);
    }else{
        $workers[$pid]=$pid;
    }
}

while(1){
    $pid=pcntl_wait($status,WUNTRACED);
    $errno=pcntl_get_last_error();
    //No child processes
    if($errno ==10){
        break;
    }
    echo $errno."-------------------------------------------".pcntl_strerror($errno).PHP_EOL;

    if($pid>0){
        echo $pid." exited!".PHP_EOL;
    }

}
