<?php
echo posix_getpid().PHP_EOL;
pcntl_async_signals( true );
// 信号处理器...
function signal_handler( $i_signo ) {
    $i_pid = posix_getpid();
    switch ( $i_signo ) {
        case SIGTERM:
            echo $i_pid." : sigterm信号.".PHP_EOL;
            break;
        case SIGUSR2:
            echo $i_pid." : sigusr2信号.".PHP_EOL;
            break;
        case SIGUSR1:
            echo $i_pid." : sigusr1信号.".PHP_EOL;
            break;
        default:
            echo "未知信号.".PHP_EOL;
    }
}
// 给进程安装信号...
pcntl_signal( SIGTERM, "signal_handler" );
pcntl_signal( SIGUSR1, "signal_handler" );
pcntl_signal( SIGUSR2, "signal_handler" );
$i_pid = pcntl_fork();
// 子进程～～～
if ( 0 == $i_pid ) {
    // while保证子进程不退出..
    echo "son:".posix_getpid().PHP_EOL;
    while ( true ) {
        sleep( 1 );
    }
}

while ( true ) {
    sleep( 1 );
}