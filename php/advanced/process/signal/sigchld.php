<?php
// fork
$i_pid = pcntl_fork();
// 下面是子进程逻辑.
if ( 0 == $i_pid ) {
    sleep( 3 );
    exit();
}

// 下面是父进程逻辑..
pcntl_async_signals( true );
// 给进程安装信号...
pcntl_signal( SIGCHLD, function( $i_signo ) use( $i_pid ) {
    switch ( $i_signo ) {
        case SIGCHLD:
            echo "收到SIGCHLD信号，有子进程退出".PHP_EOL;
            $r=pcntl_waitpid( $i_pid, $i_status, WNOHANG );
            echo  print_r($r,1).PHP_EOL;
            break;
    }
} );
// 父进程while保持不退出.
while( true ) {
    echo "before sleep".PHP_EOL;
    sleep( 100 );
    echo "after sleep".PHP_EOL;
}