<?php
/**
 * 1.信号会打断sleep
 *
 */


$pid=posix_getppid();
//父子进程均得到该信号
pcntl_signal(SIGINT,'sigfun');

$pid=pcntl_fork();
if($pid<0) return ;
$repeat=str_repeat("-",20);


if($pid>0){

    $parent_id=posix_getpid();
    cli_set_process_title("hk_parent:".$parent_id);

    pcntl_signal( SIGCHLD, function() use( $pid ) {
        echo "get son exit at ".microtime(true).PHP_EOL;
        pcntl_waitpid( $pid, $status, WNOHANG );
    } );

    $i=0;
    while( $i<10 ){
        echo $repeat."parent sleep start".$repeat.PHP_EOL;
        sleep( 10 );
        echo "parent:".$parent_id."--".$i.PHP_EOL;
        echo $repeat."parent sleep end".$repeat.PHP_EOL;
        $i++;
        echo "parent dispatch ".$i.'->'.print_r(pcntl_signal_dispatch(),1).PHP_EOL;

    }
}else if($pid==0){
    $son_pid=posix_getpid();
    cli_set_process_title("hk_son:".$son_pid);
    for($i=0;$i<5;$i++){
        echo "son:".$son_pid."--".$i.PHP_EOL;
        sleep(1);

    }
    echo "son ".$son_pid." exit at ".microtime(true).PHP_EOL;
}

function sigfun($signo){
    echo posix_getpid()." get signum: ".$signo.PHP_EOL;
    exit();
}