<?php
/**
 * Created by PhpStorm.
 * Date: 2019/5/6
 * Time: 9:12
 *
不可靠信号（在执行自定义函数其间会丢失同类信号）
可靠信号（在执行自定义函数其间不会丢失同类信号）
 *
 * 1~31 非可靠信号，不支持排队
 */
function sigfun1($signo){
    echo PHP_EOL."get sig1 ".$signo.PHP_EOL;
    sleep(1);
}
function sigfun2($signo){
    echo PHP_EOL."get sig2 ".$signo.PHP_EOL;
    sleep(1);
}


function sigfun($signum){
    pcntl_signal(SIGINT,'sigfun1');
    echo PHP_EOL."get sig ".PHP_EOL;
    sleep(2);
}


//一次只能安装1个
pcntl_signal(SIGINT,'sigfun');


while(1){
    pcntl_signal_dispatch();
    sleep(5);
}


/**
 * kill -9 `ps -ef|grep php|grep -v grep|awk  '{print $2}'|xargs`
 */

