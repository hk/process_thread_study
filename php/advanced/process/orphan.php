<?php
$pid=posix_getpid();

$id=pcntl_fork();
if($pid<0){
    echo "fork err!";
    exit();
}

if($id>0){
    cli_set_process_title("hk_parent:".posix_getpid());
    echo "父mypid:".posix_getpid()."  parent:".posix_getppid()."--sid:".posix_getsid(posix_getpid()).PHP_EOL;
    sleep(5);
}else{
    cli_set_process_title("hk_son:".posix_getpid());
    $sleep=10;
    $i=0;
    while($i<$sleep){
        echo "子mypid:".posix_getpid()."  parent:".posix_getppid()."--sid:".posix_getsid(posix_getpid()).PHP_EOL;
        sleep(1);
        $i++;
    }

}
