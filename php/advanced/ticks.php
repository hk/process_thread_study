<?php
function doTicks()
{
    echo 'Ticks'.PHP_EOL;
}

register_tick_function('doTicks');
declare(ticks = 1) {
    for ($x = 1; $x < 7; ++$x) {
        echo $x * $x .PHP_EOL;
    }
}
echo "################".PHP_EOL;
echo "1".PHP_EOL;
$a="a";
echo $a.PHP_EOL;
;
/**
1
Ticks
4
Ticks
9
Ticks
16
Ticks
25
Ticks
36
Ticks
Ticks
################
1
a
#############################
(1) 简单语句：空语句（就一个；号），return,break,continue,throw, goto,global,static,unset,echo, 内置的HTML文本，分号结束的表达式等均算一个语句。

(2) 复合语句：完整的if/elseif,while,do...while,for,foreach,switch,try...catch等算一个语句。

(3) 语句块：{} 括出来的语句块。

(4) 最后特别的：declare块本身也算一个语句(按道理declare块也算是复合语句，但此处特意将其独立出来)

 *
 */