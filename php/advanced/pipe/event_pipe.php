<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/3/4
 * Time: 14:51
 */


$mode=0666;
$eventConfig=new EventConfig;
$eventBase = new EventBase($eventConfig);

$pipe1="/tmp/pipe1";



if(!file_exists($pipe1)) {
    umask(0);
    var_dump(posix_mkfifo($pipe1,$mode));
}

$handle1 = fopen($pipe1,"r");
stream_set_blocking($handle1,0);


$event1 = new Event($eventBase, $handle1, Event::READ | Event::PERSIST,
    function ($handle) {
        echo "pipe1 come".PHP_EOL;
        while($msg=fread($handle,5)){
            echo "pipe1==".var_export($msg,1).PHP_EOL;
        }
    }, $handle1);


$event1->add();


$pipe2="/tmp/pipe2";
if(!file_exists($pipe2)) {
    umask(0);
    var_dump(posix_mkfifo($pipe2,$mode));
}

$handle2 = fopen($pipe2,"r");
stream_set_blocking($handle2,0);

$event2 = new Event($eventBase, $handle2, Event::READ | Event::PERSIST,
    function ($handle) {
        echo "pipe2 come".PHP_EOL;
        while($msg=fread($handle,5)){
            echo "pipe2==".var_export($msg,1).PHP_EOL;
        }
    }, $handle2);

$event2->add();



$eventBase->loop();