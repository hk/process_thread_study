<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/10/11
 * Time: 23:01
 */


function createPipe($pipe="/tmp/pipe"){
    $mode=0600;
    if(!file_exists($pipe)) {
        umask(0);
        posix_mkfifo($pipe,$mode);
    }
    return $pipe;
}