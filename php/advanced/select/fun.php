<?php

const HOST="0.0.0.0";
const PORT="40000";

function socktonum($sock_arr){
    $str='';
    foreach ($sock_arr as $sock){
        $str.=intval($sock).",";
    }
    $str=trim($str,',');
    return $str;
}
function sayWelcome($client){
    $buf=date("H:i:s")." welcome to server! you id:".intval($client).PHP_EOL;
    socket_write($client,$buf,strlen($buf));
}
function sigProcess($signo){
    echo $signo;
    global $clients;
    global $connect_sock;
    $bye="bye";
    foreach ($clients as $client){
        if($client ===$connect_sock) continue;
        socket_write($client,$bye,strlen($bye));
        socket_shutdown($client);
    }
    socket_shutdown($connect_sock);
    exit();
}
function sigFun($signo){
    echo posix_getpid()."  get:".$signo.PHP_EOL;
}
