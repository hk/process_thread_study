<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2019/10/16
 * Time: 10:16
 */

$flags=STREAM_SERVER_BIND | STREAM_SERVER_LISTEN;

$local_socket="tcp://0.0.0.0:8071";
$context_option=[];
$context_option['socket']['backlog'] = 5;

$context=stream_context_create($context_option);
//$opts=stream_context_get_options($stream);

stream_context_set_option($context, 'socket', 'so_reuseport', 1);

$server_stream_socket=stream_socket_server($local_socket, $errno, $errmsg, $flags, $context);

$socket = socket_import_stream($server_stream_socket);

socket_set_option($socket, SOL_SOCKET, SO_KEEPALIVE, 1);
socket_set_option($socket, SOL_TCP, TCP_NODELAY, 1);


if (!$server_stream_socket) {
    echo "$errmsg ($errno)<br />\n";
} else {
    while ($conn = stream_socket_accept($server_stream_socket)) {

        fwrite($conn, 'Welcome NOW IS ' . date('Y-m-d H:i:s').PHP_EOL );
        while($message= fread($conn, 1024)){
            echo "recv:".$message.PHP_EOL;
        }

    }
    fclose($socket);
}