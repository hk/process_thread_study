<?php
/**
 * Created by PhpStorm.
 * User: 764432054@qq.com
 * Date: 2020/3/4
 * Time: 13:26
 */



include 'common.php';


$eventConfig=new EventConfig;
$eventConfig->avoidMethod("epoll");
$eventBase = new EventBase($eventConfig);

$event = new Event($eventBase, $handle, Event::READ | Event::PERSIST,
    function ($fd) {
        echo PHP_EOL;
        sleep(1);
    }, $handle);

$event->add();
$eventBase->loop();