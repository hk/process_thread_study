<?php
$host = '0.0.0.0';
$port = 8080;
$listen_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
socket_bind($listen_socket, $host, $port);
socket_listen($listen_socket);

echo PHP_EOL . "http server on:http://{$host}:{$port}" . PHP_EOL;

socket_set_nonblock($listen_socket);

$eventBase = new EventBase();

$event = new Event($eventBase, $listen_socket, Event::READ | Event::PERSIST,
    function ($listen_socket) {
        if (($connect_socket = socket_accept($listen_socket)) != false) {
            echo "new connect " . intval($connect_socket) . PHP_EOL;
            $content="Hi ".date("Y-m-d H:i:s").PHP_EOL;
            $msg = "HTTP/1.0 200 OK\r\nContent-Length:".strlen($content)."\r\n\r\n".$content;
            socket_write($connect_socket, $msg, strlen($msg));
            socket_close($connect_socket);
        }
}, $listen_socket);
$event->add();
$eventBase->loop();













































