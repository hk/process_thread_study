<?php

echo "####".posix_getpid().PHP_EOL;

\pcntl_signal(SIGINT, SIG_IGN, false);
// uninstall graceful stop signal handler
\pcntl_signal(SIGTERM, SIG_IGN, false);
// uninstall reload signal handler
\pcntl_signal(SIGUSR1, SIG_IGN, false);
// uninstall graceful reload signal handler
\pcntl_signal(SIGQUIT, SIG_IGN, false);
// uninstall status signal handler
\pcntl_signal(SIGUSR2, SIG_IGN, false);
// uninstall connections status signal handler
\pcntl_signal(SIGIO, SIG_IGN, false);


$eventConfig=new EventConfig();

$eventBase=new EventBase($eventConfig);
$i=0;
//信号
$event1=new Event($eventBase,SIGINT,Event::SIGNAL|Event::PERSIST,function()use(&$i){
    echo date("H:i:s")." signal Int   i=".$i.PHP_EOL;
    $i++;
    if($i>10){
        exit();
    }

});
$event1->add();

$signalEvent=Event::signal($eventBase,SIGUSR1,function ($signo)use(&$i){

    echo "signo=".$signo."  i=".$i.PHP_EOL;
    $i++;
    if($i>10){
        exit();
    }
});
$signalEvent->add();



//定时器
/*$timeEvent1=new Event($eventBase,-1,Event::TIMEOUT|Event::PERSIST,function()use(&$i){
    echo date("H:i:s")."---".$i.PHP_EOL;
    if($i%5==0){
        posix_kill(posix_getpid(),SIGUSR1);
    }
    $i++;

},$i);

$timeEvent1->add(1);*/

$eventBase->loop();






