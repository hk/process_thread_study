<?php
pcntl_signal( SIGALRM, function(){
    echo "signal  ".date("H:i:s").PHP_EOL;
    sleep(3);
} );
// 定义一个时钟间隔时间，1秒钟吧
$tick = 1;
while( true ){
    // 当过了tick时间后，向进程发送一个alarm信号
    pcntl_alarm( $tick );
    // 分发信号，呼唤起安装好的各种信号处理器
    pcntl_signal_dispatch();
    echo date("H:i:s").PHP_EOL;
    // 睡个1秒钟，继续
    sleep( $tick );
}



