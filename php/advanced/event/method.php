<?php
// 查看当前系统平台支持的IO多路复用的方法都有哪些
print_r(Event::getSupportedMethods() );

$eventBase=new EventBase();
echo "当前event的方法是:".$eventBase->getMethod().PHP_EOL;

$eventConfig=new EventConfig;
$eventConfig->avoidMethod("epoll");

$eventBase=new EventBase($eventConfig);

echo "当前event的方法是:".$eventBase->getMethod().PHP_EOL;

