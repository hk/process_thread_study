<?php

//epoll不支持常规文件
$fd=fopen('./f.txt',"r");
$eventConfig=new EventConfig;
$eventConfig->avoidMethod("epoll");
$eventBase = new EventBase($eventConfig);

$event = new Event($eventBase, $fd, Event::READ | Event::PERSIST,
    function ($fd) {
       echo PHP_EOL;
        sleep(1);
    }, $fd);
$event->add();
$eventBase->loop();

