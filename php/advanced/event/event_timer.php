<?php
echo date("H:i:s").PHP_EOL;
$base = new EventBase();

$e1 = Event::timer($base, function()use(&$e1)  {
    echo "------------e1:".date("H:i:s").PHP_EOL;
    $e1->addTimer(3);

},10);
$e2 = Event::timer($base, function()use(&$e2)  {
    echo "e2:".date("H:i:s").PHP_EOL;
    $e2->addTimer(10);

},10);

//多久开始进入循环 必须的
$e1->addTimer(1);
$e2->addTimer(2);

$base->loop();
