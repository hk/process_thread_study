<?php

$eventConfig=new EventConfig();

$eventBase=new EventBase($eventConfig);

//信号
$event1=new Event($eventBase,SIGINT,Event::SIGNAL|Event::PERSIST,function(){
    echo date("H:i:s")." signal Int".PHP_EOL;
});
$event1->add();

//定时
$event2=new Event($eventBase,-1,Event::PERSIST|Event::TIMEOUT,function(){
    echo "timer1 ".date("H:i:s").PHP_EOL;

});
$event2->add(2);

//定时
$event3=new Event($eventBase,-1,Event::PERSIST|Event::TIMEOUT,function(){
    echo "timer2---------------".date("H:i:s").PHP_EOL;

});
$event3->add(3);


echo "Begin loop".PHP_EOL;
$eventBase->loop();