<?php
/**
 * Created by PhpStorm.
 * Date: 2020/3/19
 * Time: 9:31
 */
namespace singleton;

class Singleton
{
    private static $_instance;

    private function __construct()
    {
    }
    public function __clone()
    {
        throw new \Exception("can not clone");
    }
    public static function instance(){
        if(empty(self::$_instance)){
            self::$_instance=new self();
        }
        return self::$_instance;
    }

}