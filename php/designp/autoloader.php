<?php
/**
 * Created by PhpStorm.
 * Date: 2020/3/19
 * Time: 9:36
 */
function autoload($class){
    $file= dirname(__FILE__).DIRECTORY_SEPARATOR.$class.".php";
    $file=str_replace("\\",DIRECTORY_SEPARATOR,$file);
    if(!is_file($file) || !file_exists($file)){
        throw new \Exception($file." not exists");
    }
    require_once $file;
}
spl_autoload_register('autoload');