
#include "semaphore.h"

extern semid;



int main(int argc, char const *argv[])
{
	int i =0;
	int ret =0;
	int fd =-1;

	int semnum_buf[1]={0};
	//创建信号量集合
	semid=creat_or_get_sem(NSEMS);
	//初始化信号量 把编号为0 的设置为1，编号为1,2的设置为0 
	for(;i<NSEMS;i++){
		if(i ==0 ) init_sem(semid,i,1);
		else init_sem(semid,i,0);
	}

	ret =fork();
	if(ret >0 ){
		ret = fork();
		if(ret >0 ){
			while(1){
				semnum_buf[0]=2;
				sem_p(semid,semnum_buf,1);
				printf("33333\n");
				sleep(1);
				semnum_buf[0]=0;
				sem_v(semid,semnum_buf,1);
			}
		}else if(ret == 0){
			while(1){
				semnum_buf[0]=1;
				sem_p(semid,semnum_buf,1);
				printf("22222\n");
				sleep(1);
				semnum_buf[0]=2;
				sem_v(semid,semnum_buf,1);
			}
		}
		
	}else if(ret == 0){
		signal(SIGINT,signal_fun);
		while(1){
				semnum_buf[0]=0;
				sem_p(semid,semnum_buf,1);
				printf("11111\n");
				sleep(1);
				semnum_buf[0]=1;//信号量编号
				sem_v(semid,semnum_buf,1);
			}
			
	}
	return 0;
}