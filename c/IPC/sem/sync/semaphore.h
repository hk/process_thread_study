#ifndef H_SEM_H
#define H_SEM_H
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define NSEMS 3

extern void print_err(char *estr);
extern int creat_or_get_sem(int nsems);
extern void init_sem(int semid,int semnum,int val);
extern void del_sem(int semid);
extern void signal_fun(int signo);
void sem_p(int semid,int semnunm_buf[],int nsops);
void sem_v(int semid,int semnunm_buf[],int nsops);
int semid;

#endif