
#include "semaphore.h"

//extern semid;

int main(int argc, char const *argv[])
{
	int ret =0;
	int fd =-1;

	fd =open("./file",O_RDWR|O_CREAT|O_TRUNC,0664);

	if(fd ==-1) print_err("open err");
	semid = creat_or_get_sem(NSEMS);
	int i=0;
	for(i=0;i<NSEMS;i++){
		init_sem(semid,i,1);
	}
	int semnum_buf[1]={0};
	ret =fork();
	if(ret >0 ){
		
		signal(SIGINT,signal_fun);
		while(1){
			semnum_buf[0]=0; 	//设置要操作的信号量的编号
			sem_p(semid,semnum_buf,1);
			write(fd,"php",3);
			write(fd,"java",4);
			sem_v(semid,semnum_buf,1);
			
		}
	}else if(ret == 0){

			creat_or_get_sem(NSEMS);
			while(1){
				semnum_buf[0]=0; //设置要操作的信号量的编号
				sem_p(semid,semnum_buf,1);
				write(fd,"css",3);
				write(fd,"html",4);
				sem_v(semid,semnum_buf,1);
					
				//sleep(2);
		}
	}
	return 0;
}