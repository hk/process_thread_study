
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>



void print_err(char *estr){
	perror(estr);
	exit(-1);
}

int main(int argc, char const *argv[])
{
	int ret =0;
	int fd =-1;

	fd =open("./file",O_RDWR|O_CREAT|O_TRUNC,0664);

	if(fd ==-1) print_err("open err");

	ret =fork();
	if(ret >0 ){
		pid_t pid=getpid();
		char str[30]={0};
		char pid_t_str[10]={0};
		sprintf(pid_t_str,"%d",pid);
		strcat(str,"php");
		strcat(str,pid_t_str);
		
		while(1){

			write(fd,"php",3);
			write(fd,"java",4);

			//sleep(1);
			
		}
	}else if(ret == 0){
			pid_t pid=getpid();
			char str[30]={0};
			char pid_t_str[10]={0};
			sprintf(pid_t_str,"%d",pid);
			strcat(str,"css");
			strcat(str,pid_t_str);
			while(1){
				write(fd,"css",3);
				write(fd,"html",4);
				
				//sleep(2);
		}
	}
	return 0;
}