#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <signal.h>

int main(int argc, char const *argv[])
{
	int ret=0;
	int pipefd1[2]={0};//0 读，1写
	int pipefd2[2]={0};//0 读，1写
	ret = pipe(pipefd1);
	if(ret == -1){
		perror("pipe err:");
		exit(1);
	}
	ret = pipe(pipefd2);
	if(ret == -1){
		perror("pipe err:");
		exit(1);
	}
	ret = fork();
	if(ret >0 ){
		close(pipefd1[0]);
		close(pipefd2[1]);
		 char buf[30]={0};
		while(1){
			write(pipefd1[1],"hello",5);
			sleep(1);
			bzero(buf, sizeof(buf));
			read(pipefd2[0],buf,sizeof(buf));
			printf("parent recv:%s\n",buf);
		}
	}else if(ret == 0){
		 close(pipefd1[1]);
		 close(pipefd2[0]);
		 char buf[30]={0};
		while(1){
			 bzero(buf, sizeof(buf));
			 read(pipefd1[0],buf,sizeof(buf));
			 printf("child recv: %s\n",buf );
			 sleep(1);
			 write(pipefd2[1],"world",5);
			
		}
	}


	return 0;
}


