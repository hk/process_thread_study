#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <signal.h>
#include <time.h>


void fun(int signum){
	printf("signum=%d\n",signum );
}

/**
单个管道为何不能实现双工
子进程:
*/

int main(int argc, char const *argv[])
{
	int ret=0;
	int pipefd[2]={0};//0 读，1写
	ret = pipe(pipefd);
	if(ret == -1){
		perror("pipe err:");
		exit(1);
	}
	ret = fork();
	if(ret >0 ){
		while(1){
			close(pipefd[0]);
			printf("parent start writing\n");
			write(pipefd[1],"hello",5);
			int i=0;
			for(;i<5;i++){
				printf("parent sleep %d \n",i );
				sleep(1);//父进程休眠时，子进程可能被执行到
			}
			
			printf("-------------------------\n");
		}
	}else if(ret == 0){
		while(1){
			 char buf[30]={0};
			 bzero(buf, sizeof(buf));
			 printf("son start reading\n");
			 read(pipefd[0],buf,sizeof(buf));
			 printf("child recv: %s\n",buf );
			 sleep(1);
		}
	}


	return 0;
}


