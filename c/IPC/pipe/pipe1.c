#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <signal.h>

void fun(int signum){
	printf("signum=%d\n",signum );
}


int main(int argc, char const *argv[])
{
	int ret=0;
	int pipefd[2]={0};//0 读，1写
	ret = pipe(pipefd);
	if(ret == -1){
		perror("pipe err:");
		exit(1);
	}
	ret = fork();
	if(ret >0 ){
		while(1){
			signal(SIGPIPE,fun);
			close(pipefd[0]);
			write(pipefd[1],"hello",5);
			sleep(1);
		}
	}else if(ret == 0){
		close(pipefd[1]);
		close(pipefd[0]);
		while(1){
			 char buf[30]={0};
			 bzero(buf, sizeof(buf));
			 read(pipefd[0],buf,sizeof(buf));
			 printf("child recv: %s\n",buf );
			 sleep(1);
		}
	}


	return 0;
}


