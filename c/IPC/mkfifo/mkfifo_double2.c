#include "mkfifo_double.h"
/**
借助2个有名管道实现双工通信
管道1：进程1读，进程2写
管道2：进程1写，进程2读
由于每个进程的读都是阻塞的，所以，进程需要创建子进程 负责管道每个管道的读写
父进程读，子进程写
**/

int main(int argc, char const *argv[])
{
	
	int ret= -1;
	int fd1 =-1;
	int fd2 =-1;
	fd1= create_open_fifo(FIFONAME1,O_WRONLY);
	fd2= create_open_fifo(FIFONAME2,O_RDONLY);
	char buf[100]={0};
	int r=0;

	ret =fork();
	if(ret >0 ){
		signal(SIGINT,fun);
		//父进程读管道2
		while(1){
			bzero(buf, sizeof(buf));
			r=read(fd2,buf,sizeof(buf));
			if(r == 0){
				break;
			}
			printf("pid=%d,r=%d,receive:[ %s]\n",getpid(),r,buf );
		}

	}else if(ret == 0){
		char pid_str[100]={0};
		sprintf(pid_str,"---from %d:----",getpid());
		//子进程写管道1
		while(1){
			bzero(buf, sizeof(buf));
			scanf("%s",buf);
			strcat(buf,pid_str);
			write(fd1,buf,sizeof(buf));
		}
	}else{
		perror("fork err:");
		exit(-1);
	}


	return 0;
}


