#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define FIFONAME "./fifo"

void fun(int signum){
	remove(FIFONAME);
	exit(-1);
}

void print_err(char *estr){
	perror(estr);
	exit(-1);

}
int create_open_fifo(char *fifoname,int open_mode){
	int ret = -1;
	int fd =-1;
	printf("before mkfifo\n");
	ret = mkfifo(fifoname,0644);
	if( ret ==-1 && errno != EEXIST) print_err("mkfifo err");
	printf("before open\n");
	fd= open(fifoname,open_mode);
	printf("after open\n");
	if(fd == -1) print_err("open fail ");
	return fd;

}