#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define FIFONAME1 "./fifo1"
#define FIFONAME2 "./fifo2"

void fun(int signum){
	remove(FIFONAME1);
	remove(FIFONAME2);
	exit(-1);
}

void print_err(char *estr){
	perror(estr);
	exit(-1);

}
int create_open_fifo(char *fifoname,int open_mode){
	int ret = -1;
	int fd =-1;

	ret = mkfifo(fifoname,0644);
	if( ret ==-1 && errno != EEXIST) print_err("mkfifo err");

	fd= open(fifoname,open_mode);
	if(fd == -1) print_err("open fail ");
	return fd;

}