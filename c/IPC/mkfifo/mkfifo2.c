#include "mkfifo.h"

int main(int argc, char const *argv[])
{
	signal(SIGINT,fun);
	int ret=0;
	int fd =0;
	fd = create_open_fifo(FIFONAME,O_RDONLY);//只写方式打开会阻塞，所以要在其之前安装信号
	char buf[100]={0};
	int r=0;
	while(1){
		bzero(buf, sizeof(buf));
		r=read(fd,buf,sizeof(buf));
		if(r == 0){
			break;
		}
		printf("r=%d,receive: %s\n",r,buf );

	}
	return 0;
}


