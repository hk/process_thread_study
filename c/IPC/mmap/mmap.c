#include <unistd.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


void print_err(char *str,int line,int err_no){
    printf("%d,%s:%s\n",line,str,strerror(err_no) );
    exit(-1);
}

int main(int argc, char const *argv[])
{
    int srcfd=-1;
    int dstfd=-1;

    void *srcaddr=NULL;
    void *dstaddr=NULL;


    srcfd=open("/etc/passwd",O_RDWR);
    if(srcfd ==-1) print_err("open srcfd err",__LINE__,errno);

    dstfd=open("./file_dst",O_RDWR|O_CREAT|O_TRUNC);
    if(dstfd ==-1) print_err("open dst err",__LINE__,errno);

    struct stat src_stat={0};
    fstat(srcfd,&src_stat);
    srcaddr=mmap(NULL,src_stat.st_size,PROT_READ,MAP_SHARED,srcfd,0);
    if(srcaddr ==(void *) -1) print_err("mmap srcfile err",__LINE__,errno);

    ftruncate(dstfd,src_stat.st_size);
    /*
     mmap参数含义:
     1.指定映射的起始虚拟地址，如果为NULL表示由mmap指定
     2.要映射的长度
     3.指定映射后的操作权限
     4.是否立即更新到文件中
     5.要映射的文件fd
     6.指定一个偏移，表示要从文件的什么位置开始映射
    */
    dstaddr=mmap(NULL,src_stat.st_size,PROT_WRITE,MAP_SHARED,dstfd,0);
    if(dstaddr ==(void *) -1) print_err("mmap dstfile err",__LINE__,errno);

    memcpy(dstaddr,srcaddr,src_stat.st_size);


    return 0;
}