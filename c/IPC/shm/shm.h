#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <strings.h>
#include <string.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define SHM_FILE "./shmfile"
#define SHM_SIZE 4096


int shmid= -1;
void *shmaddr = NULL;

void print_err(char *estr){
	perror(estr);
	exit(-1);
}
void create_or_get_shm(void){
	int fd =0;
	key_t key =-1;
	fd =open(SHM_FILE,O_RDWR|O_CREAT,0664);
	if(fd == -1) print_err("open fail");
	key = ftok(SHM_FILE,'b');
	if(key == -1) print_err("ftok fail");

	shmid=shmget(key,SHM_SIZE,0664|IPC_CREAT);
	if(shmid == -1) print_err("shmget err");
}

char buf[300]={0};

void signal_fun(signum){
	shmdt(shmaddr);
	shmctl(shmid,IPC_RMID,NULL);
	remove(SHM_FILE);
	exit(-1);
}