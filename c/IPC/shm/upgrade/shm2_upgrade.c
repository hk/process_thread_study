#include "shm.h"
int main(int argc, char const *argv[])
{
	signal(SIGINT,signal_fun);
	//注册一个空捕获，用于唤醒pause函数
	signal(SIGUSR1,signal_fun);

	//使用有名管道，将当前进程的pid 发送给写共享内存的进程
	snd_self_pid();
	printf("mypid=%d\n",getpid() );

	//创建获取共享内存
	create_or_get_shm();
	//建立映射
	shmaddr=shmat(shmid,NULL,0);

	if(shmaddr == (void *) -1) print_err("shmaddr error");

	while(1){
		pause();
		printf("recv data:%s\n",(char *)shmaddr);
		bzero(shmaddr,SHM_SIZE);
	}

	return 0;
}

