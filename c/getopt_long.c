#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h> 

int main(int argc,char *argv[])
{
	  int opt;
	  struct option longopts[] = {
	    {"initialize",0,NULL,'i'},
	    {"file",1,NULL,'f'},
	    {"list",0,NULL,'l'},
	    {"restart",0,NULL,'r'},
	    {"help",0,NULL,'h'},
		{"output",1,NULL,'o'},
		{"version",0,NULL,'v'}
	   
	};

	  while((opt = getopt_long(argc, argv, "if:lrho:v", longopts, NULL)) != -1){
	    switch(opt){
		    case ':':
		      printf("option needs a value\n");
		      break;
		    case '?':
		      printf("unknown option: %c\n",optopt);
		      break;
	        default:
	        	printf("opt=%c,optind=%d,optarg=%s\n",opt,optind,optarg);
	        }
	    
	    
	}
}
/*

[root@centos1 c]# ./getopt_long     -v -i --file a.php -l --eee -o
opt=v,optind=2,optarg=(null)
opt=i,optind=3,optarg=(null)
opt=f,optind=5,optarg=a.php
opt=l,optind=6,optarg=(null)
./getopt_long: unrecognized option '--eee'
unknown option: 
./getopt_long: option requires an argument -- 'o'
unknown option: o



*/