#include <stdio.h>
//结构体取值复习
int main(int argc, char const *argv[])
{
	struct stu
	{
		char *name;
		int num;
		float score;	
	} stu1;
	stu1.name="hkui";
	stu1.num=100;
	stu1.score=89.5;
	struct stu *s=&stu1;
	//不可以stu1->name
	printf("name=%s,age=%d,score=%4.2f\n",stu1.name,s->num,(*s).score);

	return 0;
}
