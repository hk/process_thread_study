#include <stdio.h>
union _u{
	char a; //1字节
	int b;  //4字节
	long c;  //8字节
	void *d;  //8字节
	int e;  //4字节
	char *f; //8字节
} u;

int main(int argc, char const *argv[])
{
	printf("u len=%d\n",sizeof(u));//8
}