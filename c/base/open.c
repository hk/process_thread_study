// man 3 printf
#include <stdio.h>
//man 2 open 获取
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//man 2 write(或read)
#include <unistd.h>
//man 3 errno
#include <errno.h>

#include <stdlib.h>

//man 2 strerror或strlen
#include <string.h>

int main(void){
    int fd=0;
    fd=open("./test.txt",O_RDWR|O_CREAT,0664);
    if(-1 == fd){
        fprintf(stderr, "%s\n",strerror(errno));
        exit(1);
    }
    printf("open ok\n");
    char buf1[]="php is the best language!";
    write(fd,buf1,strlen(buf1));
    lseek(fd,SEEK_SET,0); //文件指针回到文件头，否者下面的read读不到内容

    char buf2[30]={0};
    read(fd,buf2,sizeof(buf2));
    close(fd);
    printf("buf2=%s\n",buf2 );
}