#include <stdio.h>

struct Person
{
	char name[20];
	int age;
	int sex;
	
};

int main(int argc, char const *argv[])
{
	struct Person p1={"hk",28,1};
	struct Person *p=&p1;
	printf("p1.name=%s,(*p).name=%s,p->name=%s\n",p1.name ,(*p).name,p->name);

	struct Person p_arr[2]={{"hk1",28,1},{"hk2",30,0}};
	printf("p_arr.name=%s\n",p_arr->name );
	struct Person *pp;
	pp=p_arr;
	printf("pp.name=%s\n",pp->name );
	pp++;
	printf("pp.name=%s\n",pp->name );

	//p_arr++; //常量不可运算

}
 
