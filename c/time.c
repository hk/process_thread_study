#include <stdio.h>
#include <time.h>
char *timestr(){
	time_t timep;
	static char timestr[50];
  	struct tm *p;
  	time(&timep);
  	p = gmtime(&timep);
  	sprintf(timestr,"%d:%d:%d", p->tm_hour, p->tm_min, p->tm_sec);
  	return timestr;
}
main(){
  printf("time=%s\n",timestr());
}
