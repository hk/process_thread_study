#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

void signal_fun(int signo){
	sigset_t set;
	printf("sig=%d,time up\n",signo);
	sigemptyset(&set);
	sigaddset(&set,SIGINT);//SIGINT 设置为1
	sigprocmask(SIG_UNBLOCK,&set,NULL);
	printf("sig end!\n");
}

int main(int argc, char  * const argv[])
{
	int ret=10;
	
	signal(SIGINT,signal_fun);
	
	while(1);

	return 0;
}

