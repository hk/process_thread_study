#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

void sigHandler(int signalNum){
	printf("pro signal=%d,pid=%d\n",signalNum,getpid());
}

int main(int argc, char const *argv[])
{
	signal(SIGINT,sigHandler);
	while(1);
	return 0;
}