#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

void signal_fun(int signo){
	printf("sig=%d,time up\n",signo);
}

void signal_fun1(int signo){
	printf("sig=%d,time up\n",signo);
}
void signal_fun2(int signo){
	printf("sig=%d,fun2 time up\n",signo);
}
int main(int argc, char  * const argv[])
{
	int ret=0;
	
	signal(SIGINT,signal_fun2);
	lable: ret = pause();
	 if(ret == -1 && errno == EINTR){  //当被信号唤醒后会返回-1，表示失败了，errno的错误号被设置EINTR（表示函数被信号中断）
	 	printf("goto lable\n");
	 	goto lable;
	 }
	
	printf("hello\n");
	while(1);

	return 0;
}

