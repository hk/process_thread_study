#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>


void sigHandler(int signalNum){
	printf("signal=%d,pid=%d\n",signalNum,getpid());
}


int main(int argc, char  * const argv[])
{
	pid_t ret=0;
	
	signal(SIGINT,sigHandler);//子进程不会继承
	//signal(SIGINT,SIG_IGN); 
	
	ret=fork();
	if(ret >0){
		
	}else{
		execv("./pro",argv);
	}
	while(1);

	return 0;
}

