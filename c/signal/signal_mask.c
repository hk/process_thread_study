#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

void signal_fun(int signo){

	printf("\nsig=%d,time up\n",signo);
	sleep(3);
	printf("sig end!--------------------------------------\n");
}

int main(int argc, char  * const argv[])
{
	int ret=10;
	
	signal(SIGINT,signal_fun);
	
	while(1);

	return 0;
}

