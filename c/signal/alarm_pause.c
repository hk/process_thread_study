#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

void signal_fun(int signo){
	printf("sig=%d,time up\n",signo);
}

void signal_fun1(int signo){
	printf("sig=%d,time up\n",signo);
}

int main(int argc, char  * const argv[])
{
	unsigned int ret=0;
	
	signal(SIGALRM,signal_fun);
	ret=alarm(5);
	printf("ret=%d\n",ret );
	sleep(2);
	ret=alarm(6);
	printf("ret=%d\n",ret );
	
	// signal(SIGINT,signal_fun1);
	// pause();
	printf("hello\n");
	
	while(1);

	return 0;
}

