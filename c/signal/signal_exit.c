#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
typedef void (*sighandler_t)(int);


void sigHandler(int signalNum){
	printf(" sign  = %d\n",signalNum);
	exit(-1);

}
void process_exit_fun(void){
	printf("process over\n");
}
int main(){
	sighandler_t ret=NULL;
	ret=signal(SIGINT,sigHandler);
	atexit(process_exit_fun);
	printf("end -----");
	while(1);
		
	return 0;
}
/*
[root@centos1 signal]# ./a.out 
^Cend ----- sign  = 2
process over


*/