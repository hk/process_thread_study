#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

void signal_fun(int signo){
	printf("sig=%d,time up\n",signo);
}


int main(int argc, char  * const argv[])
{
	int ret=10;
	
	signal(SIGINT,signal_fun);

	lable: ret = sleep(ret); //被信号打断时  返回剩余的秒数
	 if(ret != 0){
	 	printf("ret=%d\n",ret );
	 	goto lable;
	 }
	
	printf("hello\n");
	while(1);

	return 0;
}

