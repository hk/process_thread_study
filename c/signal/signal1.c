#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
typedef void (*sighandler_t)(int);


void sigHandler(int signalNum){
	printf(" sign  = %d\n",signalNum);

}
int main(){
	sighandler_t ret=NULL;
	ret=signal(SIGINT,SIG_IGN);
	printf("ret=%d,SIG_DFL=%d\n",ret,SIG_DFL );
	ret=signal(SIGINT,sigHandler);
	printf("ret=%d,SIG_IGN=%d\n",ret,SIG_IGN);
	ret=signal(SIGINT,SIG_DFL);
	printf("ret=%p\n",ret );
	while(1);
		
	return 0;
}