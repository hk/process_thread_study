#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

typedef void (*sighandler_t)(int);
void sigHandler(int signalNum){
	printf("child signal=%d,pid=%d\n",signalNum,getpid());
}
void sigHandler1(int signalNum){
	printf("parent signal=%d,pid=%d\n",signalNum,getpid());
}

int main(int argc, char const *argv[])
{
	pid_t ret=0;
	
	signal(SIGINT,sigHandler);
	signal(SIGINT,SIG_DFL);
	ret=fork();
	if(ret >0){
		signal(SIGINT,sigHandler1);
	}else{
		signal(SIGINT,sigHandler);
	}
	while(1);

	return 0;
}

