#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

void signal_fun(int signo){
	printf("sig=%d,time up\n",signo);
}

int main(int argc, char  * const argv[])
{
	int ret=10;
	
	signal(SIGINT,signal_fun);
	char buf[100]={0};
	read(0,buf,sizeof(buf));
	printf("hello\n");
	while(1);

	return 0;
}

