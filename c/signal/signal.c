#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <string.h>


void sigHandler(int signalNum){
	printf("the sign  start %d\n",signalNum);
	sleep(1);
	printf("the sign   end %d\n",signalNum);

}
int main(){
    printf("pid=%d\n",getpid());
	signal(SIGINT,sigHandler);
	signal(SIGSEGV,sigHandler);
	//signal(SIGQUIT,SIG_IGN);   //忽略 ^\信号
	//signal(SIGQUIT,sigHandler); 
	while(1);
		
	return 0;
}