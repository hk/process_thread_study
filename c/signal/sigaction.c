#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

void sig_handler(int,siginfo_t*,void*);

int main(int argc,char** argv){
	struct sigaction act;
	sigset_t newmask,oldmask;
	int rc;
	sigemptyset(&newmask);//清空信号集中所有信号
	sigaddset(&newmask,SIGINT);//信号集中添加一个非实时信号
	sigaddset(&newmask,SIGRTMIN);//信号集里添加一个实时信号
	sigprocmask(SIG_BLOCK,&newmask,&oldmask);//屏蔽实时信号SIGRTMIN和非实时信号SIGRTMIN,并将之前的信号集保存到oldmask中
	act.sa_sigaction=sig_handler;
	act.sa_flags=SA_SIGINFO;
	if(sigaction(SIGINT,&act,NULL) <0 ){
		printf("install signal error\n");
	}
	if(sigaction(SIGRTMIN,&act,NULL) <0 ){
		printf("install signal error\n");
	}
	printf("my pid=%d\n",getpid() );
	//进程睡眠60s，在此时间内，发给改进程的所有实时信号将排队,信号不会丢失
	sleep(60);
	//解除SIGRTMIN和SIGINT信号的屏蔽，此时其信号处理函数被调用，对
	//于可靠信号,有几个信号，其信号处理函数就调用几次,对于不可靠信号,只调用一次
	sigprocmask(SIG_SETMASK,&oldmask,NULL);
	return 0;

}
void sig_handler(int signum,siginfo_t* info,void* myact){
	if(signum == SIGINT){
		printf("Got a common signal SIGINT\n");
	}else{
		printf("Got a real time signal SIGRTMIN \n");
	}
}