#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <string.h>

#define INPUTLEN 20



void inthandler(int s){
	printf("Received signal %d ... waiting\n", s);
	sleep(2);
	printf("Leaving  inthandler\n");
	signal(SIGINT,inthandler);
}

void quithandler(int s){
	printf("Received signal %d ... waiting\n", s);
	sleep(3);
	printf("Leaving  quithandler\n");
	signal(SIGQUIT,quithandler);
}

void main(){
	signal(SIGINT,inthandler);
	signal(SIGQUIT,quithandler);
	int nchars;
	char input[INPUTLEN];
	do{
		printf("please intput a message\n");
		nchars=read(0,input,(INPUTLEN-1));
		if(nchars == -1){
			perror("read return an error");
		}else{
			input[nchars]='\0';
			printf("you have inputed:%s\n",input);
		}
	}while(strncmp(input,"quit",4) !=0 );
}