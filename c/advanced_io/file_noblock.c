#include <unistd.h>
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>



void print_err(char *str,int line,int err_no){
	printf("%d,%s:%s\n",line,str,strerror(err_no) );
	exit(-1);
}

int main(int argc, char const *argv[])
{
	

	char buf[100]={0};
	int fd =-1;
	int ret= 0;

	fd =open("./file",O_RDONLY|O_CREAT,0664); 
	if(fd == -1) print_err("open fail",__LINE__,errno);

	while(1){
		printf("begin\n");
	
		ret =read(fd,buf,sizeof(buf)); 
		if(ret >0 )printf("%s\n", buf);
		
		printf("end\n");


	}
	return 0;
}