#include <stdio.h>
#include <stdlib.h>

#define Size 4

typedef struct Table{
	int *head;
	int length;
	int size; //顺序表分配存储的容量
} table;

table initTable(){
	table t;
	t.head=(int*) malloc(Size*sizeof(int));
	if(!t.head){
		printf("初始化失败\n");
		exit(0);
	}
	t.length=0;
	t.size=Size;
	return t;
}
int main(){
	table t=initTable();
	printf("Size=%d\n",sizeof(t)); //16
}

