#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void child(int delay){
    sleep(delay);
    exit(0);
}

void parent(int *status){
    wait(status);
}

main(){
    pid_t pid;
    int status;
    printf("Before:%d\n",getpid());
    pid=fork();
    if(pid == 0){
        child(10);
    }
    if(pid >0 ){
        printf("pid =%d\n",getpid());
        parent(&status);
        printf("status =%d\n",status);
    }
}
/*
[root@centos1 c]# ./a.out
Before:14901
pid =14901
status =0


*/