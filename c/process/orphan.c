#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void main(){
	pid_t pid;
	pid=fork();
	int i=0;
	if(pid == 0){
		while(i<10){
			printf("pid=%d,ppid=%d\n",getpid(),getppid() );
			sleep(1);
			i++;
		}
	}else if(pid>0){
		sleep(5);
	}
	printf("%d over\n",getpid());
}

/**
父进程比子进程先退出,父进程退出后，子进程被1号进程收养
父进程退出后，shell拿到终端控制权
输出例子
[root@centos1 process]# gcc orphan.c 
[root@centos1 process]# ./a.out 
pid=53269,ppid=53268
pid=53269,ppid=53268
pid=53269,ppid=53268
pid=53269,ppid=53268
pid=53269,ppid=53268
53268 over
[root@centos1 process]# pid=53269,ppid=1
pid=53269,ppid=1
pid=53269,ppid=1
pid=53269,ppid=1
pid=53269,ppid=1
53269 over

*/