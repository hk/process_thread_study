
#include <stdio.h>
#include <stdlib.h>

getids(){

	printf("pid:%d,ppid:%d,gid:%d,sid:%d\r\n",getpid(),getppid(),getgid(),getsid(getpid()));
}
main(){
	int ret_from_fork;
	printf("Before :my pid is %d \n   ",getpid() ); 
	ret_from_fork=fork();
	if(ret_from_fork >0){
		printf("parent_pid:%5d,ret_from_fork=%5d\n",getpid(),ret_from_fork);
		sleep(3);
		exit(1);
	}else if(ret_from_fork ==0 ){
		printf("child_pid:%5d,ret_from_fork=%5d\n",getpid(),ret_from_fork);
		int i=0;
		for(;i<10;i++){
			getids();
			
			sleep(1);
		}
	}
	
}


