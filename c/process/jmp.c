#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

jmp_buf jmp;
int g_v=1;

void fun(){
	printf("jmp=%d\n",jmp );
	longjmp(jmp,1);
	printf("jmp=%d\n",jmp );
}

int main(void){
	static int s_v=1;
	auto int a_v=1;
	register r_v=1;
	volatile v_v=1;
	int *h_v=(int*)malloc(sizeof(int));
	*h_v=1;
	printf("1:g_v=%d,s_v=%d,a_v=%d,r_v=%d,v_v=%d,h_v=%d\n",g_v,s_v,a_v,r_v,v_v,*h_v );
	printf("setjmp(jmp)=%d\n",setjmp(jmp));
	if(setjmp(jmp) == 1){
		printf("2:g_v=%d,s_v=%d,a_v=%d,r_v=%d,v_v=%d,h_v=%d\n",g_v,s_v,a_v,r_v,v_v,*h_v );
		exit(1);
	}

	g_v=2;
	s_v=2;
	a_v=2;
	r_v=2;
	v_v=2;
	*h_v=2;
	printf("3:g_v=%d,s_v=%d,a_v=%d,r_v=%d,v_v=%d,h_v=%d\n",g_v,s_v,a_v,r_v,v_v,*h_v );
	fun();
	return 0;



}