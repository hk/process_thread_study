#include <stdlib.h>
#include <stdio.h>
void process_end1(void){
	printf("deal 1\n");
}
void process_end2(void){
	printf("deal 2\n");
}
int main(int argc, char const *argv[])
{
	/* code */
	atexit(process_end1);
	atexit(process_end2);
	printf("over\n");

	return 0;
}

