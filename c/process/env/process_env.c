#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

extern char **environ;

void showenv(char **env){
	int i=0;
	char *str;
	while ((str =env[i]) != NULL){
		printf("%s\n",str );
		i++;

	}
}
int main(int argc,char *argv[],char *envp[]){
	printf("envrison=%p,envp=%p\n",environ,envp );//2个变量地址值一样的
	//showenv(environ);
	//printf("-------\n");
	//showenv(envp);
	printf("----------------------------\n");
	char *oldpath=getenv("PATH");
	char *addpath=":/tmp/hkui";
	int newlen=strlen(oldpath)+1+strlen(addpath)+1;
	printf("newlen=%d\n",newlen );
	char newpath[newlen];
	strcpy(newpath,oldpath);
	strcat(newpath,addpath);


	printf("oldpath:%s\n",oldpath);
	setenv("PATH",newpath,1);
	printf("newpath:%s\n",getenv("PATH"));
	putenv("NAME=HKUI2018");
	printf("NAME:%s\n",getenv("NAME"));


	return 0;
}