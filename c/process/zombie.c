#include <stdio.h>
#include <unistd.h>

void parent_code(int delay){
    sleep(delay);
}
main(){
    pid_t pid;
    int status;

    pid=fork();
    //僵尸
    if(pid == 0){
        int i=0;
        for(;i<5;i++){
            printf("pid=%d,ppid=%d\n",getpid(),getppid() );
            sleep(1);
        }
         printf("pid=%d end\n",getpid() );

    }else if(pid>0){
        parent_code(10);
        printf("pid=%d,ppid=%d  over\n",getpid(),getppid() );

    }
    

}
