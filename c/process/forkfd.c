#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>

int main(){
    char buf[10];
    char *str1="This is child process";
    char *str2="This is parent process";
    pid_t pid;
    int fd,readsize;
    fd=open("test.txt",O_WRONLY);
    if(fd == -1){
        perror("open failed");
        exit(0);
    }
    readsize=read(fd,buf,5);//读取aaaaa字符串到buf,此时与fd关联的文件位置指针指向了第二行的第一个字符
    pid=fork();//子进程此时复制了fd的属性,其与父进程fd的文件位置指针指向同一个地方
    switch(pid){
        case -1:
                perror("fork failed");
                exit(0);
                break;
        case 0:
                write(fd,str1,strlen(str1));
                break;
        default:
                write(fd,str2,strlen(str2));
    }
}
/**
创建子进程时,子进程也继承了父进程所打开的文件描述符,当刚创建结束时
父子进程共享了文件描述符的文件位置指针
即父进程对文件描述符读写操作后,子进程的文件位置指针也跟着变化,因此务必要注意

可能出现
This is parThis is parent process ent process


*/