#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>

#define MAXFILE 65535
char *timestr();
int main(){
	pid_t pc;
	char wtr[100];
	int i,fd,len;
	char *buf ="hello everybody!";
	len=strlen(buf);
	pc=fork();
	if(pc<0){
		printf("fork err\n");
		exit(1);
	}
	if(pc>0) exit(0);
	

	setsid();
	chdir("/");
	umask(0);
	for(i=0;i<MAXFILE;i++){
		while(1){
			if(fd= open("/tmp/daemon.log",O_CREAT|O_WRONLY|O_APPEND,0600)<0){
				perror("open");
				exit(1);
			}
			sprintf(wtr, "%s-%s\n",buf,timestr());
			write(fd,wtr,strlen(wtr)+1);
			close(fd);
			sleep(2);
		}
		
	}
}

char *timestr(){
	time_t timep;
	static char timestr[50];
  	struct tm *p;
  	time(&timep);
  	p = gmtime(&timep);
  	sprintf(timestr,"%d:%d:%d", p->tm_hour, p->tm_min, p->tm_sec);
  	return timestr;
}