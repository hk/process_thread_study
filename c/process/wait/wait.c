#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char **environ;

int main(int argc, char const *argv[])
{
	pid_t ret = 0;
	ret = fork();
	if(ret >0){
		printf("parent pid=%d\n", getpid());
		int status=0;
		wait(&status);//阻塞等待子进程死
		printf("status=%d\n",status); // 1->256
		if(WIFEXITED(status)){
			printf("exited :%d\n", WEXITSTATUS(status));
		}else if(WIFSIGNALED(status)){
			printf("signal killed:%d\n",WTERMSIG(status) );
		}
	}else if(ret == 0){
		printf("son pid=%d\n", getpid());
		execv("./wait_return",argv,environ); 
	}
	return 0;
}