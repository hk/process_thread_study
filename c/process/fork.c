#include <stdio.h>

main(){
	int ret_from_fork;
	printf("Before :my pid is %d \n   ",getpid() ); //加\n和不加的区别(子进程复制父进程的数据 缓存)
	ret_from_fork=fork();
	if(ret_from_fork >0){
		printf("parent_pid:%5d,ret_from_fork=%5d\n",getpid(),ret_from_fork);
	}else if(ret_from_fork ==0 ){
		printf("child_pid:%5d,ret_from_fork=%5d\n",getpid(),ret_from_fork);
	}
	printf("pid=%d %s\n",getpid(),"end");
	while (1);
}
