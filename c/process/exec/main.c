#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	pid_t ret = 0;
	printf("this_pid:%d\n",getpid() );
	
	ret = fork();
	if(ret >0){
		printf("-------------------------------------------------parent--start----\n" );
		sleep(2);
		printf("----------------------------------------------------parent--end----\n" );
	}else if(ret == 0){
		extern char **environ;
		execv("./pro",argv,environ);
	}
	return 0;
}
