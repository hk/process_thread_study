#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[],char **environ)
{
	int i = 0;
	printf("pid:%d----parent_id:%d\n",getpid(),getppid() );
	printf("argv:\n");
	for(i=0;i<argc;i++){
		printf("%s\n",argv[i] );
	}
	printf("--------------------\nenviron:\n");

	for(i=0 ;NULL != environ[i];i++){
		printf("%s\n",environ[i] );
	}

	printf("\n-----end-------\n");
	return 0;
}