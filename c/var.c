#include <sys/types.h>  //pid_t 类型的在此
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>   //exit 用

int glob=6;  //全局变量

int main(){
	int local;
	pid_t pid;
	local = 8;
	if( (pid=fork()) == 0){
		sleep(2);
	}else if(pid>0){
		glob =60;
		local =80;
		sleep(10);
	}
	printf("glob=%d,local=%d,mypid=%d\n",glob,local,getpid());
	exit (0);
}
/*
[root@centos1 process]# ./var
glob=6,local=8,mypid=22800
glob=60,local=80,mypid=22799


*/