#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(){
	int fd=0;
	fd=open("./r.txt",O_RDWR|O_CREAT|O_TRUNC,0664);
	if(fd ==-1){
		perror("open error:");
		exit;
	}
	char buf[]="hello world";
	int n=0;
	int offset=2;
	//n=write(fd,buf,strlen(buf)); //ok
	
	//n=write(fd,buf+offset,strlen(buf)-offset);  //ok

	n=write(fd,"hello world"+offset,strlen("hello world")-offset);
	if(n== strlen("hello world")-offset){
		printf("write ok\n");
	}else{
		printf("write err:n=%d\n",n);
		perror("write err:");
	}
	return 0;
}

