#include "io.h"
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#define BUFF_LEN 1024

void copy(int fd_in,int fd_out){
	char buff[BUFF_LEN]={"\0"};
	ssize_t n;
	//返回读到的字节数，若已到文件尾为0，出错返回-1
	while((n=read(fd_in,buff,BUFF_LEN))!=0){
		if(n<0){
			fprintf(stderr, "read failed\n", strerror(errno));
		}else if(n>0){
			//返回成功写入的字节数，若出错返回-1
			if(write(fd_out,buff,n)!=n){
				fprintf(stderr, "write failed\n", strerror(errno));
			}else{

			}
		}
	}

}

