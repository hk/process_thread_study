#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	printf("S_IRUSR=%d\n",S_IRUSR );
	printf("S_IWUSR=%d\n",S_IWUSR );
	printf("S_IXUSR=%d\n",S_IXUSR );

	if(argc <2){
		printf("need file name \n");
		exit(1);
	}
	int ret=0;
	char *f=(char *)argv[1];
	printf("f=%s\n",f );
	struct stat sta={0};
	// ret= stat(f, &sta);
	ret= lstat(argv[1],  &sta);
	if(-1 == ret){
		perror("stat err:");
		exit(1);
	}
	printf("%d %lu %d %d %ld %ld %s\n",
		sta.st_mode,sta.st_nlink,sta.st_uid,sta.st_gid,sta.st_size,sta.st_atime,f );
	char file_type='0';
	if(S_ISLNK(sta.st_mode)) 		file_type='l';
	else if(S_ISREG(sta.st_mode)) 	file_type='r';
	else if(S_ISBLK(sta.st_mode)) 	file_type='b';
	else if(S_ISDIR(sta.st_mode)) 	file_type='d';
	else if(S_ISCHR(sta.st_mode)) 	file_type='c';
	else if(S_ISFIFO(sta.st_mode)) 	file_type='p';
	else if(S_ISSOCK(sta.st_mode)) 	file_type='s';
	printf("file_type=%c\n",file_type );

	//打印文件权限

	char buf[10]={0};
	char tmp_buf[]="rwxrwxrwx";
	int i=0;
	for(i=0;i<9;i++){
		if(sta.st_mode & (1<<(8-i))){ 
			buf[i]=tmp_buf[i];
		}
		else buf[i]='-';
	}
	printf("%s\n",buf );
	return 0;
}

      
