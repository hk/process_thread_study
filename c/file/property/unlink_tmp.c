#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define F "./tmp.txt"

int main(int argc, char const *argv[])
{
	int fd=0;
	fd=open(F,O_RDWR|O_CREAT,0777);
	if(fd == -1){
		perror("open fail:");
		exit(1);
	}
	int r=0;
	r=unlink(F);
	if(-1 == r){
		perror("unlink error:");
		exit(1);
	}
	char buf[10]={0};
	write(fd,"php",3);
	lseek(fd,0,SEEK_SET);

	read(fd,buf,sizeof(buf));

	printf("buf=%s\n",buf );
	return 0;
}