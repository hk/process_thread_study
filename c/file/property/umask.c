#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(){
	int fd=0;
	mode_t ret=0;
	ret=umask(0);
	printf("old_mask=%d\n",ret );
	fd=open("/tmp/new_file.txt",O_RDWR|O_CREAT,0777);
	if(-1 == fd){
		perror("open fail:");
		return 0;
	}
	ret=umask(ret);
	printf("ret=%d\n",ret );
	return 0;
}





