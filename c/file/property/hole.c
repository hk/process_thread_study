#include <unistd.h>
#include <sys/types.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#define FILE_NAME "./hole.txt"


int main(int argc, char const *argv[])
{
	
	int fd=0;
	fd =open(FILE_NAME,O_RDWR| O_CREAT,0644);
	if(fd == -1){
		perror("open err:");
		exit(1);

	}
	//truncate(FILE_NAME,2048);
	lseek(fd,2048,SEEK_SET);
	write(fd,"php",3);


	return 0;
}