#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

int main(){
	int fd=0;
	fd=open("./file.txt",O_RDWR|O_CREAT,0777);
	//truncate("./file.txt",10);
	ftruncate(fd,120); //超过文件长度时成为空洞文件
	return 0;
}