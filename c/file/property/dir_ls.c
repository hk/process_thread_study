#include <sys/types.h>
#include <dirent.h>
#include <error.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

int main(int argc, char const *argv[])
{
    DIR *dirp=NULL;
    dirp=opendir(".");
    if(NULL ==dirp){
        perror("opendir fail");
        exit(-1);
    }
    while(1){
        struct dirent *direntp=NULL;
        direntp=readdir(dirp);
        if(direntp == NULL && errno !=0){
            perror("readdir err");
            exit(-1);
        }
        
        if(direntp == NULL && errno ==0) break;
        printf("inodeID=%lu,fname=%s\n",direntp->d_ino,direntp->d_name );
    }
    return 0;
}




