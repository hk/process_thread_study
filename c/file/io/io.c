#include "lock.h"
#include <unistd.h>
#include <fcntl.h>
int lock_reg(int fd,int cmd,short type,short whence ,off_t l_start,off_t len){
	struct flock lock;
	lock.l_type=type;
	lock.l_start=l_start;
	lock.l_whence=whence;
	lock.l_len=len;
	return (fcntl(fd,cmd,&lock));

}