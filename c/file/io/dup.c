/**
文件描述符操作 dup和dup2
#include <unistd.h>
int dup(int fieldes)
int dup2(int fields,int fieldes2)
返回：成功返回新文件描述符，出错返回-1
可用来复制文件描述符
在进程间通信返回新的文件描述符，出错返回-1
*/
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main(int argc,char *argv[]){
	int fd1=0;
	int fd2=0;
	int fd3=0;
	fd1=open("./file.txt",O_RDWR|O_TRUNC|O_CREAT,0644);
	fd2=dup(fd1);
	fd3=dup2(fd1,6);
	printf("fd1=%d,fd2=%d,fd3=%d\n",fd1,fd2,fd3 );
	write(fd1,"hello\n",6);
	write(fd2,"world\n",6);
	write(fd3,"php\n",4);
	return 0;
}
