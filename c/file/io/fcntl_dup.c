#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


int main(int argc,char *argv[]){

	int fd=0;
	
	fd=open("./file.txt",O_RDWR|O_TRUNC);
	if(-1 == fd){
		perror("open err");
		return 0;
	}


	// close(1);
	// fcntl(fd,F_DUPFD,0); //0表示不用第三个参数

	//模拟dup2
	 close(1);
	fcntl(fd,F_DUPFD,1);

	printf("aaaa\n");
	
	return 0;
}

