#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


int main(void){
	int fd=0;
	fd=open("./file.txt",O_RDWR);
	if(-1 == fd){
		fprintf(stderr, "%s\n",strerror(errno));
		exit(1);
	}
	printf("open ok\n");
	char buf1[]="hello word";
	write(fd,buf1,strlen(buf1));
	lseek(fd,SEEK_SET,0);

	char buf2[30]={0};
	read(fd,buf2,sizeof(buf2));
	close(fd);
	printf("buf2=%s\n",buf2 );
}

