/**
阻塞io模型
*/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

int main(){
	size_t n; //8字节
	char buffer[100]={0};
	int i=0;
	while((n=read(STDIN_FILENO,buffer,100))!=0){  //换行时读取
		i++;
		printf("\n%d----------%d-------------------\n",i,n );
		if(write(STDOUT_FILENO,buffer,n)!=n){
				fprintf(stderr, "write error:%s\n",strerror(errno));
				exit(1);
		}
	}
	return 0;
}