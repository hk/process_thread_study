#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc,char *argv[]){
	int fd1=0;
	int fd2=0;
	
	fd1=open("./txt_dup_relocate.txt",O_RDWR|O_CREAT,0644);
	// close(1);
	// dup(fd1);//或者 fd2=dup(fd1)   dup会找到描述符池中最小的那个 此处即为1 拿来复制返回给fd2即为1

	dup2(fd1,1);
	
	printf("fd1=%d,fd2=%d\n",fd1,fd2 ); //此时printf的输出就被重定位到了 txt_dup_relocate.txt里了
	
	return 0;
}
