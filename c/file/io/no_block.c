#include <unistd.h>  //read函数、write函数和getpid函数 sleep,文件权限相关的
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

int set_flag(int fd,int flag){
	int var = fcntl(fd,F_GETFL);
	var |=flag;
	if(fcntl(fd,F_SETFL,var)<0){
		return 0;
	}
	return 1;
}

int main(){
	set_flag(STDIN_FILENO,O_NONBLOCK);

	size_t n;
	char buffer[100]={0};
	sleep(5);
	while((n=read(STDIN_FILENO,buffer,100))!=-1){
		printf("n=%d\n", n);
		if(n == 0)
			printf("finished\n"); // ctrl+d
		else if(n>0){
			if((write(STDOUT_FILENO,buffer,n))!=n){
				fprintf(stderr, "write error%s\n",strerror(errno));
				exit(1);
			}
		}
	}
	return 0;
}


