#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/*

fcntl(int fd,int cmd)
返回:成功则依赖于cmd,出错为-1
*/
int set_flag(int fd,int flag){
	int var = fcntl(fd,F_GETFL);
	var |=flag;
	if(fcntl(fd,F_SETFL,var)<0){
		return 0;
	}
	return 1;
}

int main(int argc,char *argv[]){

	int fd=0;
	
	fd=open("./file.txt",O_RDWR|O_TRUNC);
	if(-1 == fd){
		perror("open err");
		return 0;
	}

	// close(1);
	// dup(fd);
	/*模拟dup*/
	// close(1);
	// fcntl(fd,F_DUPFD,0); //0表示不用第三个参数

	//模拟dup2
	// close(1);
	// fcntl(fd,F_DUPFD,1);
	int fd1=open("./file.txt",O_RDWR|O_TRUNC);
	int flag=0;
	flag=fcntl(fd,F_GETFL,0);
	printf("flag=%d\n",flag);
	flag=flag|O_APPEND;
	//设置后不再覆盖
	fcntl(fd,F_SETFL,flag);
	fcntl(fd1,F_SETFL,flag);
	while(1){
		write(fd,"php\n",4);
		sleep(1);
		write(fd1,"js\n",3);
	}
	
	return 0;
}

