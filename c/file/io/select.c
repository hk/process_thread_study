/**
select i/o多路转换
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

int main(void){
	fd_set set; //long 类型的数组，每一个数组元素都能与一打开的文件句柄（不管是socket句柄，还是其他文件或命名管道或设备句柄）建立联系
	FD_ZERO(&set);
	FD_SET(STDIN_FILENO,&set);

	struct timeval t;
	t.tv_sec=3;
	t.tv_usec=0;

	int n;
	while((n=select(STDIN_FILENO,&set,NULL,NULL,&t))!=-1){
		printf("n=%d\n",n );
		if(n==0){
			printf("timeout\n");
		}
		if(FD_ISSET(STDIN_FILENO,&set)){
			char buffer[100]={0};
			size_t m;
			m=read(STDIN_FILENO,buffer,100);
			if(write(STDOUT_FILENO,buffer,m)!=m){
				fprintf(stderr, "%s\n", strerror(errno));
				exit(1);
			}
		}
		t.tv_sec=3;
		t.tv_usec=0;

		FD_ZERO(&set);
		FD_SET(STDIN_FILENO,&set);
	}
	FD_CLR(STDIN_FILENO,&set);
	return 0;
}