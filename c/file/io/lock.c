#include "lock.h"
#include <stdio.h>
#include <stdlib.h> //exit
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>


int main(int argc,char *argv[]){
	int fd=open(argv[1],O_WRONLY);
	size_t len=strlen(argv[2]);
	WRITE_LOCKW(fd,SEEK_SET,0L,0L);
	lseek(fd,0L,SEEK_END);
	int i=0;
	for(i;i<len;i++){
		if(write(fd,&argv[2][i],1)!=1){
			fprintf(stderr, "write error:%s\n", strerror(errno));
			exit(1);
		}
		printf("%d write %c\n",getpid(),argv[2][i]);
		sleep(1);
	}
	UNLOCK(fd,SEEK_SET,0L,0L);
	close(fd);
	printf("over\n");
	return 0;

}



