#ifndef _LOCK_
#define _LOCK_
#include <sys/types.h>
int lock_reg(int fd,int cmd,short type,short l_whence ,off_t l_start,off_t len);

//非等待的共享读锁
#define READ_LOCK(fd,l_whence,l_start,len)\
	lock_reg(fd,F_SETLK,F_RDLCK,l_whence,l_start,len)
//等待的共享读锁
#define READ_LOCKW(fd,l_whence,l_start,len)\
	lock_reg(fd,F_SETLKW,F_RDLCK,l_whence,l_start,len)

//非等待的独占写锁
#define WRITE_LOCK(fd,l_whence,l_start,len)\
	lock_reg(fd,F_SETLK,F_WRLCK,l_whence,l_start,len)
//等待的独占写锁
#define WRITE_LOCKW(fd,l_whence,l_start,len)\
	lock_reg(fd,F_SETLKW,F_WRLCK,l_whence,l_start,len)

//解锁
#define UNLOCK(fd,l_whence,l_start,len)\
	lock_reg(fd,F_SETLKW,F_UNLCK,l_whence,l_start,len)
#endif