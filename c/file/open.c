/*
flags:
三选一
O_RDONLY只读模式
O_WRONLY只写模式
O_RDWR读写模式
可选
O_APPEND每次写操作都写入文件的末尾
O_CREAT如果指定文件不存在，则创建这个文件,使用此参数时需指定第三个参数指定穿建的文件的权限
O_EXCL如果要创建的文件已存在，则返回-1，并且修改errno的值，如果同时指定了O_CREAT则导致调用错误
O_TRUNC如果文件存在，并且以只写/读写方式打开，则清空文件全部内容(即将其长度截短为0)
O_NOCTTY如果路径名指向终端设备，不要把这个设备用作控制终端。
O_NONBLOCK如果路径名指向FIFO/块文件/字符文件，则把文件的打开和后继I/O
 
*/


#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc,char *argv[]){
	int fd=open("./1.txt",O_RDWR|O_CREAT|O_EXCL,0664);
	if(fd<0){
		printf("errno=%d\n",errno);
		fprintf(stderr, "err:%s\n",strerror(errno));
		perror("fd error");
	}else{
		printf("fd=%d\n",fd );
	}
	close(fd);
	
	return 0;
}