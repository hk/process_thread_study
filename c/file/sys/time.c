#include <time.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char const *argv[])
{
	 time_t timestamp=0;
	 int ret=0;
	 ret=  time(&timestamp); //或者指定为NULL
	 char *strp=NULL;
	 strp=ctime(&timestamp);

	 printf("ret=%d,timestamp=%d\n",ret,timestamp);
	 printf("strp=%s\n",strp );

	 struct tm *tmp =NULL;
	 //tmp=gmtime(&timestamp); //国际的
	 tmp=localtime(&timestamp);
	 printf("%d-%d-%d %d:%d:%d\n",tmp->tm_year,tmp->tm_mon,tmp->tm_mday,tmp->tm_hour,tmp->tm_min,tmp->tm_sec );

	 strp=asctime(tmp);
	 printf("strp=%s\n",strp );

	 char buf[100]={0};
	 strftime(buf,sizeof(buf),"%Y-%m-%d %H:%M:%S\n",tmp);
	 printf("buf=%s\n",buf );
	 return 0;
}

