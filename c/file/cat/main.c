#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc,char *argv[]){
	if(argc <2){
		fprintf(stderr, "usage:%s\n", argv[0]);
		exit(1);
	}
	int fd_in =open(argv[1],O_RDONLY);
	if(fd_in <0){
		fprintf(stderr, "open file 1 err:%s\n", strerror(errno));
		exit(1);
	}
	printf("open file 1:%d\n",fd_in );

	copy(fd_in,STDOUT_FILENO);
	close(fd_in);
	return 0;
}