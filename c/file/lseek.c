#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>


int main(){
	int fd=open("/etc/passwd",O_RDONLY);
	int size=0;
	size=lseek(fd,0,SEEK_END);//移动到文件末尾 不在偏移，返回当前读写位置相对于文件开始位置的偏移量 即为文件大小
	printf("size =%d\n",size );

	lseek(fd,-20,SEEK_END);
	char buf[30]={0};
	read(fd,buf,30);
	printf("buf=%s\n",buf );

	close(fd);

}

