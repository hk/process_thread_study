#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#define FILE_NAME "./file.txt"

void print_error(char *str){
    perror(str);
    exit(-1);
}
int main(){
    int fd1=0;
    int fd2=0;
    fd1=open(FILE_NAME,O_RDWR|O_TRUNC|O_APPEND|O_CREAT,0775);
    if(-1 == fd1) print_error("1 open error:");
    fd2=open(FILE_NAME,O_RDWR|O_APPEND|O_CREAT,0775);
    if(-1 == fd2) print_error("2 open error:");
    printf("fd1=%d,fd2=%d\n",fd1,fd2 );

    while(1){
        write(fd1,"php\n",4);
        sleep(1);
        write(fd2,"aaa\n",4);

    }

}

