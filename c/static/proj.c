#include <stdio.h>
#include <stdlib.h>

static int i=11;
int j=22;

static void func(void){
	printf("%s,i=%d\n",__FUNCTION__,i);
	printf("%s,j=%d\n",__FUNCTION__,j);
}
void call_func(){
	printf("%s\n",__FUNCTION__ );
	func();
}
/*
proj.c里定义的auto变量或者函数,main函数里识别不了，main.c里定义和proj.c里同名的变量，编译时又报重复定义的错误
只能各种里用static修饰防止编译时报重复定义和使用时找不到

static修饰函数
使当前函数只在当前的.c文件里生效


*/