#include <unistd.h>
#include <stdio.h>
#include <stdlib.h> 

main(){
	char *arglist[3];
	pid_t pid;
	arglist[0]="ls";
	arglist[1]="-l";
	arglist[2]=0;
	printf("parent:%d\n",getpid());
	pid=fork();
	if( pid==0 ){
		printf("son1  pid is:%d\n",getpid() );
		execvp("ls",arglist);
		printf("son2  pid is:%d\n",getpid() );
	}else{
		printf("parnet pid:%d\n",getpid() );
	}
	printf("in %d\n",getpid());
	
}
/**
execvp 后的当前进程的代码不再执行
[root@centos1 c]# ./exec
parent:25790
parnet pid:25790
in 25790
son1  pid is:25791
[root@centos1 c]# 总用量 17
-rwxrwxrwx 1 root root 7020 5月  13 10:37 exec
-rwxrwxrwx 1 root root  421 5月  13 10:37 exec.c
-rwxrwxrwx 1 root root 8560 5月  13 10:01 fork
-rwxrwxrwx 1 root root  447 5月  12 21:50 fork.c
-rwxrwxrwx 1 root root  487 5月  12 21:50 var.c

**/