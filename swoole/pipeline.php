<?php
set_time_limit(0);
ini_set('memory_limit','1024M');

$host='192.168.0.250';
$redis = new Redis();

G('1');
$redis->connect($host);
//不具备原子性 ,管道
$redis->pipeline();
for ($i=0;$i<100;$i++) {
    $redis->set("test_{$i}",pow($i,2));
    $redis->get("test_{$i}");
}
$redis->exec();
$redis->close();
G('1','e');

G('2');
$redis->connect($host);
//事物具备原子性
$redis->multi();
for ($i=0;$i<100;$i++) { $redis->set("test_{$i}",pow($i,2));
    $redis->get("test_{$i}");
}
$redis->exec();
$redis->close();
G('2','e');

//普通
G('3');
$redis->connect($host);
//事物具备原子性
for ($i=0;$i<100;$i++) { $redis->set("test_{$i}",pow($i,2));
    $redis->get("test_{$i}");
}
$redis->close();
G('3','e');

function G($star,$end = '')
{
    static $info = array();
    if (!empty($end))
    {
        $info[$end] = microtime(true);
        $sconds = $info[$end] - $info[$star];
        echo $sconds,"ms<br>";

    } else {
        $info[$star] = microtime(true);
    }
}

?>
