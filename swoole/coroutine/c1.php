<?php
use Swoole\Coroutine;
use function Swoole\Coroutine\run;

$urls = [
    'http://www.baidu.com',
    'http://www.sina.com.cn',
    'http://www.qq.com',
    'https://www.cnblogs.com/',
    'https://edu.51cto.com/',
    'https://blog.51cto.com/'
];
function curlData($url)
{
    echo $url."-------------------------------------------begin".PHP_EOL;
    $content=file_get_contents($url);
    return compact('url','content');
}

$t1=microtime(true);
run(function ()use($urls) {
    foreach ($urls as $url){
        Coroutine::create(function()use($url) {
            $data=curlData($url);
            echo $data['url'].PHP_EOL;
        });
    }
});
$t2=microtime(true);
echo PHP_EOL.($t2-$t1).PHP_EOL;
