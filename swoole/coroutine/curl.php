<?php
$t1=microtime(true);

$urls = [
    'http://www.baidu.com',
    'http://www.sina.com.cn',
    'http://www.qq.com',
    'https://www.cnblogs.com/',
    'https://edu.51cto.com/',
    'https://blog.51cto.com/'
];

$useProcess=0;
if(!$useProcess){
    foreach ($urls as $url) {
        $content = curlData($url);
        echo $content['url'].PHP_EOL;
    }
}


//使用swoole的process开启多个子进程
if($useProcess){
    foreach ($urls as $url){
        $process = new swoole_process(function (swoole_process $worker) use ($url) {
            //curl处理
            $content = curlData($url);
            print_r($content['url']);
        }, true);
        $pid = $process->start();
        $wokers[$pid] = $process;
    }
    foreach ($wokers as $process) {
        echo $process->read().PHP_EOL;
    }
}


function curlData($url)
{
    $content=file_get_contents($url);
    return compact('url','content');
}
$t2=microtime(true);


echo ($t2-$t1).PHP_EOL;