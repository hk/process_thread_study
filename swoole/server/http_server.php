<?php
$http = new swoole_http_server("0.0.0.0", 8080);

$http->set([
    'enable_static_handler'=>true,
    'document_root'=>__DIR__.'/data',
    'worker_num'=>5
]);

$http->on('request', function ($request, $response) {
    $response->header("Content-Type", "text/html; charset=utf-8");
    $response->end("<h4>Hello Swoole ".date("Y-m-d H:i:s")."</h4>".__DIR__);
});

$http->start();
