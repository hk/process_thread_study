<?php
$serv = new swoole_server("0.0.0.0", 9501);
$serv->set([
    'worker_num'=>4,  //worker进程数
    'max_request'=>10000,
    "reactor_num"=>3,
]);

//监听连接进入事件
$serv->on('connect', function ($serv, $fd,$reactor_id) {
    echo "Client: {$reactor_id}-{$fd} -Content.\n";
});
$serv->on('start', function ($serv) {
    echo posix_getpid().PHP_EOL;
    echo "##############################".PHP_EOL;
    echo "listen:{$serv->host}:{$serv->port}\n";
    echo "master_pid:{$serv->master_pid}".PHP_EOL;
    echo "manager_pid:{$serv->manager_pid}".PHP_EOL;

    echo "##############################".PHP_EOL;
});

//监听数据接收事件
$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
    echo $data;
    $serv->send($fd, "Server: ".$reactor_id."--".$fd."--".$data);
});

//监听连接关闭事件
$serv->on('close', function ($serv, $fd) {
    echo "Client: Close.\n";
});

//启动服务器
$serv->start();

/*
 *
 *
ps aft|grep tcp_server.php


 * netstat -anp|grep 9501
 *
 * telnet 192.168.0.250 9501
 ctrl+] 后回车


回调函数的参数

Array
(
    [0] => Swoole\Server Object
        (
            [onConnect] => Closure Object
                (
                    [parameter] => Array
                        (
                            [$serv] => <required>
                            [$fd] => <required>
                            [$reactor_id] => <required>
                        )

                )

            [onReceive] => Closure Object
                (
                    [parameter] => Array
                        (
                            [$serv] => <required>
                            [$fd] => <required>
                            [$reactor_id] => <required>
                            [$data] => <required>
                        )

                )

            [onClose] => Closure Object
                (
                    [parameter] => Array
                        (
                            [$serv] => <required>
                            [$fd] => <required>
                        )

                )

            [onPacket] =>
            [onBufferFull] =>
            [onBufferEmpty] =>
            [onStart] =>
            [onShutdown] =>
            [onWorkerStart] =>
            [onWorkerStop] =>
            [onWorkerExit] =>
            [onWorkerError] =>
            [onTask] =>
            [onFinish] =>
            [onManagerStart] =>
            [onManagerStop] =>
            [onPipeMessage] =>
            [setting] => Array
                (
                    [worker_num] => 8
                    [max_request] => 10000
                    [task_worker_num] => 0
                    [buffer_output_size] => 2097152
                    [max_connection] => 655360
                )

            [connections] => Swoole\Connection\Iterator Object
                (
                )

            [host] => 0.0.0.0
            [port] => 9501
            [type] => 1
            [mode] => 3
            [ports] => Array
                (
                    [0] => Swoole\Server\Port Object
                        (
                            [onConnect] => Closure Object
                                (
                                    [parameter] => Array
                                        (
                                            [$serv] => <required>
                                            [$fd] => <required>
                                            [$reactor_id] => <required>
                                        )

                                )

                            [onReceive] => Closure Object
                                (
                                    [parameter] => Array
                                        (
                                            [$serv] => <required>
                                            [$fd] => <required>
                                            [$reactor_id] => <required>
                                            [$data] => <required>
                                        )

                                )

                            [onClose] => Closure Object
                                (
                                    [parameter] => Array
                                        (
                                            [$serv] => <required>
                                            [$fd] => <required>
                                        )

                                )

                            [onPacket] =>
                            [onBufferFull] =>
                            [onBufferEmpty] =>
                            [onRequest] =>
                            [onHandShake] =>
                            [onMessage] =>
                            [onOpen] =>
                            [host] => 0.0.0.0
                            [port] => 9501
                            [type] => 1
                            [sock] => 3
                            [setting] => Array
                                (
                                    [worker_num] => 8
                                    [max_request] => 10000
                                )

                            [connections] => Swoole\Connection\Iterator Object
                                (
                                )

                        )

                )

            [master_pid] => 14540
            [manager_pid] => 14541
            [worker_id] => 2
            [taskworker] =>
            [worker_pid] => 14548
        )

    [1] => 1
    [2] => 2
    [3] => 1111

*/
