<?php

class AsyncMysql
{
    public $db;
    public $config;

    public function __construct($config)
    {
        $this->config = $config;
        $this->db = new swoole_mysql();

    }

    public function update()
    {

    }

    public function add()
    {

    }

    public function execute()
    {

        $this->db->connect($this->config, function ($db, $r) {
            echo "connected".PHP_EOL;
            if (false === $r) {
                var_dump($db->connect_errno, $db->connect_error);
                die;
            }
            $sql = 'show tables';

            $db->query($sql, function (swoole_mysql $db, $r) {
                echo "in query".PHP_EOL;
                if ($r === false) {
                    var_dump($db->error, $db->errno);
                } elseif ($r === true) {
                    var_dump($db->affected_rows, $db->insert_id);
                }
                print_r($r);
                $db->close();
            });
            echo __LINE__.PHP_EOL;


        });
        return true;
    }
}


$server = array(
    'host' => '192.168.0.106',
    'port' => 3306,
    'user' => 'root',
    'password' => '123456',
    'database' => 'yii2',
    'charset' => 'utf8', //指定字符集
    'timeout' => 2,  // 可选：连接超时时间（非查询超时时间），默认为SW_MYSQL_CONNECT_TIMEOUT（1.0）
);

$mysql = new AsyncMysql($server);
$r = $mysql->execute();
echo "r:" . var_export($r, true) . PHP_EOL;
echo 'end' . PHP_EOL;

